/**
 *  @file	demo_main3.cpp
 *  @brief	A demonstration file for the part main3.
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <cmath>

#include "../src/pixel.h"
#include "../src/image.h"
using std::cout;
using std::cin;
using std::endl;
using std::cerr;

//to complete

/**
 * \brief	The function main  to generate an executable of demonstration for the part main3.
 *
 */
int main(int argc, char** argv )
{
    if ( argc != 2 )
     {
         printf("usage: ./DisplayImage <Image_Path>\n");
         return -1;
     }

     Image img = Image(argv[1]);

     //TODO: on pourrait pas faire ça dans le constructeur de Image ?
     if ( !img.get_matrix().data )
     {
         printf("No image data \n");
         return -1;
     }


     Size s(15,15);
     /* Illustration of intensity effect */
     Image intensity1 = img.special_convolution(LINEAR,128,200,ID,s,BORDER_CONSTANT, 0);
     Image intensity2 = img.special_convolution(QUADRATIC,128,200,ID,s,BORDER_CONSTANT, 0);
     Image intensity3 = img.special_convolution(E_GAUSS,128,200,ID,s,BORDER_CONSTANT, 0);  
     /* 
     intensity1.save_image("images/ID_kernel_Linear_energy.png");
     intensity2.save_image("images/ID_kernel_Quad_energy.png");
     intensity3.save_image("images/ID_kernel_Gauss_energy.png");
     */
     /* Illustration of blurring effect */
     Image blurred1 = img.special_convolution(E_GAUSS,128,200,MEDIUM,s,BORDER_CONSTANT, 0);
     Image blurred2 = img.special_convolution(E_GAUSS,128,200,TRIANGLE,s,BORDER_CONSTANT, 0);
     Image blurred3 = img.special_convolution(E_GAUSS,128,200,PARABOLA,s,BORDER_CONSTANT, 0);
     Image blurred4 = img.special_convolution(E_GAUSS,128,200,K_GAUSS,s,BORDER_CONSTANT, 0);
     /* 
     blurred2.save_image("images/Triangle_blurred.png");
     blurred3.save_image("images/Parabola_blurred.png");
     blurred4.save_image("images/Gaussian_blurred.png");
     */
     /* Reverse kernel to Sharp the image knowing the blur kernel */
     unsigned int n = 7;
     Mat K = Mat::ones(n,n, CV_64F);
     K = K/(n*n);
     Mat M, M0 ;
     img.get_matrix().copyTo(M);
     Image img0 = img.cast_into_float();
     img0.get_matrix().copyTo(M0);
     Mat Opencv_border_ct;
     M.copyTo(Opencv_border_ct);
     filter2D(M,Opencv_border_ct,-1,K,Point(-1,-1),0, BORDER_CONSTANT);
     Image(Opencv_border_ct).save_image("images/Blur_image_15x15.png");
     K = Image::reverseKernel(K);
     filter2D(Opencv_border_ct,Opencv_border_ct,-1,K,Point(-1,-1),0, BORDER_CONSTANT);
     Image(Opencv_border_ct).save_image("images/Deblur_image_1.png");
     filter2D(Opencv_border_ct,Opencv_border_ct,-1,K,Point(-1,-1),0, BORDER_CONSTANT);
     Image(Opencv_border_ct).save_image("images/Deblur_image_2.png");
     filter2D(Opencv_border_ct,Opencv_border_ct,-1,K,Point(-1,-1),0, BORDER_CONSTANT);
     Image(Opencv_border_ct).save_image("images/Deblur_image_3.png");
    return 0;
}
