cmake_minimum_required (VERSION 2.8.11)
project(Demo)

find_package( OpenCV REQUIRED )

add_executable(demo_st1 demo_starter1.cpp)
add_executable(demo_st2 demo_starter2.cpp)
add_executable(demo_st3 demo_starter3.cpp)
add_executable(demo_kernel demo_suggest_kernel.cpp)
add_executable(demo_st4 demo_starter4.cpp)
add_executable(demo_main1 demo_main1.cpp)
add_executable(demo_main2 demo_main2.cpp)
add_executable(demo_main3 demo_main3.cpp)
add_executable(demo_main4 demo_main4.cpp)
add_executable(demo_minutiae demo_minutiae.cpp)

target_link_libraries(demo_st1 PUBLIC ${OpenCV_LIBRARIES} libFingerPrint)
target_include_directories(demo_st1 PUBLIC src)

target_link_libraries(demo_st2 PUBLIC ${OpenCV_LIBRARIES} libFingerPrint)
target_include_directories(demo_st2 PUBLIC src)

target_link_libraries(demo_st3 PUBLIC ${OpenCV_LIBRARIES} libFingerPrint)
target_include_directories(demo_st3 PUBLIC src)

target_link_libraries(demo_kernel PUBLIC ${OpenCV_LIBRARIES} libFingerPrint)
target_include_directories(demo_kernel PUBLIC src)

target_link_libraries(demo_st4 PUBLIC ${OpenCV_LIBRARIES} libFingerPrint)
target_include_directories(demo_st4 PUBLIC src)

target_link_libraries(demo_main1 PUBLIC ${OpenCV_LIBRARIES} libFingerPrint)
target_include_directories(demo_main1 PUBLIC src)

target_link_libraries(demo_main2 PUBLIC ${OpenCV_LIBRARIES} libFingerPrint)
target_include_directories(demo_main2 PUBLIC src)

target_link_libraries(demo_main3 PUBLIC ${OpenCV_LIBRARIES} libFingerPrint)
target_include_directories(demo_main3 PUBLIC src)

target_link_libraries(demo_main4 PUBLIC ${OpenCV_LIBRARIES} libFingerPrint)
target_include_directories(demo_main4 PUBLIC src)

target_link_libraries(demo_minutiae PUBLIC ${OpenCV_LIBRARIES} libFingerPrint)
target_include_directories(demo_minutiae PUBLIC src)
