/**
 *  @file	demo_minutiae.cpp
 *  @brief	A demonstration file expected to find the minutiae of a digital print
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <vector>
#include <math.h>

#include "../src/minutiae.h"
#include "../src/pixel.h"
#include "../src/image.h"
#include "../src/structuring_element.h"


using std::cout;
using std::cin;
using std::endl;
using std::cerr;

/**
 * \brief	The function main  to generate an executable of demonstration for the minutiae detection.
 *
 */
int main(int argc, char** argv )
{
  if ( argc != 2 )
  {
      printf("usage: feed me image\n");
      return -1;
  }

  Image img = Image(argv[1]);

  //TODO: on pourrait pas faire ça dans le constructeur de Image ?
  if ( !img.get_matrix().data )
  {
      printf("No image data \n");
      return -1;
  }

/*
  Mat blurred;
  blur(img.get_matrix(), blurred, Size(5,5), Point(-1,-1));
  Image(blurred).display_image("Blurred", 0);
  img = Image(blurred);
*/

  display_minutiae(img);

/*
  Image squeleton = img.binarize_with_threshold(70).squeletonize();
  Image pruned = squeleton.prune(3);
  img.display_image("Original image", 0);
  squeleton.display_image("Skeleton", 0);
  pruned.display_image("Pruned", 0);
  */
  return 0;
}
