/**
 *  @file	demo_main1.cpp
 *  @brief	A demonstration file for the part main1.
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <cmath>

#include "../src/pixel.h"
#include "../src/image.h"
using std::cout;
using std::cin;
using std::endl;
using std::cerr;

/**
 * \brief	The function main  to generate an executable of demonstration for the part main1.
 *
 */
int main(int argc, char** argv )
{
    if ( argc != 2 )
    {
        printf("usage: ./DisplayImage <Image_Path>\n");
        return -1;
    }

    Image img1 = Image(argv[1]);
    Image img2 = Image(argv[1]);

    //TODO: on pourrait pas faire ça dans le constructeur de Image ?
    if ( !img1.get_matrix().data )
    {
        printf("No image data \n");
        return -1;
    }

    if ( !img2.get_matrix().data )
    {
        printf("No image data \n");
        return -1;
    }

    img1.anisotropic_pressure(.5,120, 120);
    img1.display_image("Anisotropic pressure effect" , 0);

    img2.isotropic_pressure(.5,120, 120, PHYSICAL);
    img2.display_image("Isotropic pressure effect" , 0);

    return 0;
}
