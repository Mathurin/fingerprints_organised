/**
 *  @file	demo_main2.cpp
 *  @brief	A demonstration file for the part main2.
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <vector>
#include <math.h>
#include <cmath>

#include "../src/pixel.h"
#include "../src/image.h"
#include "../src/functions.h"
using std::cout;
using std::cin;
using std::endl;
using std::cerr;
using std::stof;


/**
 * \brief	The function main  to generate an executable of demonstration for the part main2.
 *
 */
int main(int argc, char** argv )
{
    if ( argc != 3 )
    {
        cout << "usage: ./demo_main2 image.png strength" << endl;
        return -1;
    }
    Image image = Image(argv[1]);
    float strength = stof(argv[2]);

     if ( !image.get_matrix().data )
     {
         printf("No image data \n");
         return -1;
     }

    if (strength < -1 || strength > 1) {
      cout << "The strength should be between -1 and 1" << endl;
      return -1;
    }

    // The center of the fingerprint is hardcoded, because
    // the function used to find the center is not very accurate.
    
    vector<float> center{140, 240};
    //vector<float> center{200, 240};
    Mat new_image = image.warp_image(center, strength);
    Image warped = Image(new_image);


    image.display_image("Original image", 0);
    warped.display_image("Warped image", 0);

    return 0;
}
