/**
 *  @file	demo_main4.cpp
 *  @brief	A demonstration file for the part main4.
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <cmath>

#include "../src/pixel.h"
#include "../src/image.h"
#include "../src/structuring_element.h"
using std::cout;
using std::cin;
using std::endl;
using std::cerr;

/**
 * \brief	The function main  to generate morphological operations on fingerprints.
 *
 */

 int main(int argc, char** argv )
 {
   if ( argc != 2 )
   {
       printf("usage: feed me image\n");
       return -1;
   }

   Image img = Image(argv[1]);

   if ( !img.get_matrix().data )
   {
       printf("No image data \n");
       return -1;
   }

   // The center of the fingerprint is hardcoded, because
   // the function used to find the center is not very accurate.
   vector<float> center{140, 240};
   /*
   vector<uint> ctr = img.find_center(0, 0, img.get_matrix().cols, img.get_matrix().rows);
   cout << "center of the fingerprint " << ctr[0] << ", " << ctr[1] << endl;
   */

   Image img1 = img.grayscale_dilate(CROSS, 3);
   Image img2 = img.real_grayscale_dilate(center);

   Image img3 = img.grayscale_erose(DIAMOND, 3);
   Image img4 = img.real_grayscale_erose(center);


   img.display_image("Original image", 0);
   img1.display_image("Uniformly Dilated image", 0);
   img2.display_image("Focused Dilated image", 0);

   img3.display_image("Uniformly Erosed image", 0);
   img4.display_image("Focused Erosed image", 0);


   return 0;
 }
