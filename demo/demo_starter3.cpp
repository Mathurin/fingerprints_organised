/**
 *  @file	demo_starter3.cpp
 *  @brief	A demonstration file for the part starter3.
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <vector>
#include <math.h>

#include "../src/pixel.h"
#include "../src/image.h"
using std::cout;
using std::cin;
using std::endl;
using std::cerr;

/**
 * \brief	The function main  to generate an executable of demonstration for the part starter3.
 *
 */
 #include "opencv2/core/core.hpp"
 #include "opencv2/imgproc/imgproc.hpp"
 #include "opencv2/highgui/highgui.hpp"
 #include <iostream>
 int main(int argc, char ** argv)
 {
     if ( argc != 2 )
     {
         printf("usage: ./DisplayImage <Image_Path>\n");
         return -1;
     }

     Image img = Image(argv[1]);

     //TODO: on pourrait pas faire ça dans le constructeur de Image ?
     if ( !img.get_matrix().data )
     {
         printf("No image data \n");
         return -1;
     }

     unsigned int n = 7;
     Mat K = Mat::ones(n,n, CV_64F);
     K = K/(n*n);
     Mat M, M0 ;
     img.get_matrix().copyTo(M);
     Image img0 = img.cast_into_float();
     img0.get_matrix().copyTo(M0);
     Mat Opencv_border_ct, Opencv_border_ct0;
     Mat Opencv_border_replicate, Opencv_border_replicate0;
     Mat Opencv_border_reflect, Opencv_border_reflect0;
     M.copyTo(Opencv_border_ct);
     M.copyTo(Opencv_border_replicate);
     M.copyTo(Opencv_border_reflect);
     M0.copyTo(Opencv_border_ct0);
     M0.copyTo(Opencv_border_replicate0);
     M0.copyTo(Opencv_border_reflect0);

     cout << endl;
     cout << "================================="<< endl;
     cout << "Convolution with border constant = 0 " << endl;
     cout << "---------------------------------"<< endl;
     /*  border constant int convention*/
     filter2D(M,Opencv_border_ct,-1,K,Point(-1,-1),0, BORDER_CONSTANT);
     Image(Opencv_border_ct).save_image("images/openCV_border_ct_filter.png");
     Image img_spat_ct = img.convolution_spatial(K, BORDER_CONSTANT);
     img_spat_ct.save_image("images/filter_using_spatial_convolution_border.png");
     cout << "Value of accuracy in int: " ;
     cout << img_spat_ct.compare_image(Opencv_border_ct) << endl;

     cout << "================================="<< endl;
     cout << "Convolution with border replicate " << endl;
     cout << "---------------------------------"<< endl;
     /*  border replicate int convention*/
     filter2D(M,Opencv_border_replicate,-1,K,Point(-1,-1), 0, BORDER_REPLICATE);
     Image(Opencv_border_replicate).save_image("images/Opencv_border_replicate_filter.png");
     Image img_spat_repl = img.convolution_spatial(K, BORDER_REPLICATE);
     img_spat_repl.save_image("images/filter_using_spatial_convolution_border_replicate.png");
     cout << "Value of accuracy in int: " ;
     cout << img_spat_repl.compare_image(Opencv_border_replicate) << endl;

     cout << "================================="<< endl;
     cout << "Convolution with border reflect " << endl;
     cout << "---------------------------------"<< endl;
     /*  border reflect int convention*/
     filter2D(M,Opencv_border_reflect,-1,K,Point(-1,-1), 0, BORDER_REFLECT);
     Image(Opencv_border_reflect).save_image("images/openCV_border_reflect_filter.png");
     Image img_spat_ref = img.convolution_spatial(K, BORDER_REFLECT);
     img_spat_ref.save_image("images/filter_using_spatial_convolution_border_reflect.png");
     cout << "Value of accuracy in int: ";
     cout << img_spat_ref.compare_image(Opencv_border_reflect) << endl;

     /*  border reflect float convention*/
     filter2D(M0,Opencv_border_reflect0,-1,K,Point(-1,-1), 0, BORDER_REFLECT);
     Image(Opencv_border_reflect0).cast_into_int().save_image("images/openCV_border_reflect_filter0.png");

     cout << "================================="<< endl;
     cout << "Convolution with border reflect vs fft" << endl;
     cout << "---------------------------------"<< endl;
     /*  border reflect int convention*/
     Image img_freq = img.convolution_frequency(K);
     img_freq.save_image("images/filter_using_fft.png");
     cout << "Value of accuracy in int: ";
     cout  << img_freq.compare_image(Opencv_border_reflect) << endl;

     /*  border reflect float convention*/
     Image img_freq0 = img.convolution_frequency(K, false);
     img_freq0.cast_into_int().save_image("images/filter_using_fft0.png");
     cout << "Value of accuracy in float: ";
     cout  << img_freq0.compare_double_image(Opencv_border_reflect0) << endl;
     cout << endl;

     cout << "Computation in float & recast in int at end: ";
     cout << img_freq0.cast_into_int().compare_image(Image(Opencv_border_reflect0).cast_into_int().get_matrix()) << endl;
     cout << endl;

     // cout << "Value of accuracy in int when cutting border: ";
     unsigned int r = 0;
     unsigned int w = img_freq.get_matrix().cols;
     unsigned int h = img_freq.get_matrix().rows;
     Image img_r = img_freq.resized(Rect(r, r, w-2*r, h-2*r));
     Mat reflect_r = Opencv_border_reflect(Rect(r, r, w-2*r, h-2*r));
     Image diff_r = img_r.diff_contraste(reflect_r);
     string s = "r = " +  std::to_string(r) ;
     diff_r.save_image("images/contraste_cut"+s+".png");
     diff_r.display_image(s, 0);


     Image diff = img_freq.diff_contraste(Opencv_border_reflect);
     diff.save_image("images/contraste.png");
     Image diff1 = img_spat_ct.diff_contraste(img_spat_repl.get_matrix());
     diff1.save_image("images/comparison_ct_repl.png");
     Image diff2 = img_spat_ct.diff_contraste(img_spat_ref.get_matrix());
     diff2.save_image("images/comparison_ct_ref.png");
     Image diff3 = img_spat_ref.diff_contraste(img_spat_repl.get_matrix());
     diff3.save_image("images/comparison_ref_repl.png");

     return 0;
 }
