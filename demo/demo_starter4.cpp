/**
 *  @file	demo_starter4.cpp
 *  @brief	A demonstration file for the part starter4.
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <vector>
#include <math.h>

#include "../src/pixel.h"
#include "../src/image.h"
#include "../src/structuring_element.h"
using std::cout;
using std::cin;
using std::endl;
using std::cerr;

/**
 * \brief	The function main  to generate an executable of demonstration for the part starter4.
 *
 */
int main(int argc, char** argv )
{
  if ( argc != 2 )
  {
      printf("usage: feed me image\n");
      return -1;
  }

  Image img = Image(argv[1]);

  //TODO: on pourrait pas faire ça dans le constructeur de Image ?
  if ( !img.get_matrix().data )
  {
      printf("No image data \n");
      return -1;
  }

  Image img1 = img.binarize();
  Image img2 = img1.dilate(CIRCLE, 3);
  Image img3 = img1.erose(CIRCLE, 3);
  Image img4 = img1.hit_or_miss();
  Image img5 = img1.squeletonize();

  img.display_image("Original image", 0);
  img1.display_image("Binarized", 0);
  img2.display_image("Dilated", 0);
  img3.display_image("Erosed", 0);
  img4.display_image("Hit_or_miss", 0);
  img5.display_image("skeleton", 0);

  return 0;
}
