/**
 *  @file	demo_suggest_kernel.cpp
 *  @brief	A demonstration file for the suggestion of the kernel in starter3.
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <vector>
#include <math.h>

#include "../src/pixel.h"
#include "../src/image.h"
using std::cout;
using std::cin;
using std::endl;
using std::cerr;

/**
 * \brief	The function main  to generate an executable of demonstration for kernel suggestion of the part starter3.
 *
 */
 #include "opencv2/core/core.hpp"
 #include "opencv2/imgproc/imgproc.hpp"
 #include "opencv2/highgui/highgui.hpp"
 #include <iostream>
 int main()
 {
     Image img_clean = Image("images/clean_finger.png");
     Image img_blur = Image("images/blurred_finger.png");

     for (unsigned int n=1; n<15; n++){
       cout << "Size of the kernel :  " << n << endl;
       Mat K = Mat::ones(n,n, CV_32F);
       K = K/(n*n);

       Image img_freq = img_clean.convolution_frequency(K);

       cout << "Value of accuracy: blurred finger & the clean one : ";
       cout << img_blur.compare_image(img_clean.get_matrix()) << endl;

       cout << "Value of accuracy: blurred finger & our fft convolution : ";
       cout << img_blur.compare_image(img_freq.get_matrix()) << endl;
       cout << endl;
     }
     return 0;
}
