/**
 *  @file	demo_starter2.cpp
 *  @brief	A demonstration file for the part starter2.
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <vector>
#include <math.h>

#include "../src/pixel.h"
#include "../src/image.h"
#include "../src/functions.h"
using std::cout;
using std::cin;
using std::endl;
using std::cerr;
using std::stof;

/**
 * \brief	The function main to rotate an image by a certain given angle.
 *
 */
int main(int argc, char** argv )
{
    if ( argc != 3 )
    {
        cout << "usage: ./demo_starter2 <Image_path> <angle>" << endl;
        return -1;
    }
    Mat image;
    image = imread( argv[1], CV_LOAD_IMAGE_GRAYSCALE);
    float angle = stof(argv[2]);

    if ( !image.data )
    {
        cout << "No image data \n" << endl;
        return -1;
    }

    Image original = Image(image);

    // rotation part
    Mat new_image = original.rotated_image(angle);

    // translation part
    vector<float> t{0,0};
    Image rotated = Image(new_image);
    Mat new_new_image = rotated.translated_image(t);


    // Correct rotated image by OpenCV
    vector<float> center{(float)image.cols/2, (float)image.rows/2};
    Mat image1 = Mat(image.rows, image.cols, image.type(), Scalar(255));
    Mat mat_rot = getRotationMatrix2D(Point(center[0], center[1]), angle, 1);
    warpAffine(image, image1, mat_rot, image.size(), INTER_CUBIC, BORDER_TRANSPARENT, Scalar(255));
    Image correct_image = Image(image1);

    //float accuracy = correct_image.compare_image(new_image);
    //cout << "accuracy: " << accuracy << " %" << '\n';

    imshow("original Image", image);
    imshow("Correct rotated Image", image1);
    imshow("Rotated Image", new_new_image);

    waitKey(0);

    return 0;
}
