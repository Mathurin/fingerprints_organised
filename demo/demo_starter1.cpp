/**
 *  @file	demo_starter1.cpp
 *  @brief	A demonstration file for the part starter1.
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <vector>
#include <math.h>

#include "../src/pixel.h"
#include "../src/image.h"
using std::cout;
using std::cin;
using std::endl;
using std::cerr;

/**
 * \brief	The function main  to generate an executable of demonstration for the part starter1.
 *
 */
int main(int argc, char** argv )
{
    if ( argc != 2 )
    {
        printf("usage: ./DisplayImage <Image_Path>\n");
        return -1;
    }

    Image img = Image(argv[1]);

    //TODO: on pourrait pas faire ça dans le constructeur de Image ?
    if ( !img.get_matrix().data )
    {
        printf("No image data \n");
        return -1;
    }

    img.display_informations();
    Mat m = img.get_matrix();
    Size s = m.size();
    Image img_bis = img.draw_rectangle(s.width/1.45, s.width/1.27, s.height/1.45, s.height/1.12, 0);
    img.save_image("images/Test.png");
    Image img_ter = img_bis.draw_rectangle(s.width/8.4, s.width/2.1, s.height/2.5, s.height/1.6, 255);
    img_ter.display_image("Draw of rectangle", 0);
    img_ter.save_image("images/Two_rectangles.png");
    Image img_4 = img.symmetry_along_y_axis();
    img_4.save_image("images/Symetry_along_y.png");
    Image img_5 = img.symmetry_along_diag1();
    img_5.save_image("images/Symetry_along_diag1.png");
    Image img_6 = img.symmetry_along_x_axis();
    img_6.save_image("images/Symetry_along_x.png");
    Image img_7 = img.symmetry_along_diag2();
    img_7.save_image("images/Symetry_along_diag2.png");
    Image img_8 = img.periodized();
    img_8.save_image("images/Periodized_image.png");

    return 0;
}
