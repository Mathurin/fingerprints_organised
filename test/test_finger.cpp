//Standard includes
#include <iostream>
#include <cstdint>
#include <vector>
#include <math.h>
//Opencv includes
#include <opencv2/opencv.hpp>

#include "gtest/gtest.h"
# include "../src/image.h"
# include "../src/pixel.h"


class ImageTest: public ::testing::Test {
protected:
    virtual void SetUp() {}
    virtual void TearDown() {
    // Code here will be called immediately after each test
    // (right before the destructor).
    }
};

TEST_F(ImageTest,constructor){
Image img = Image("../images/clean_finger.png");
EXPECT_EQ(256,img.width);
EXPECT_EQ(288, img.height);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
