/**
 *  @file	anisopressure.h
 *  @brief	Implement a class ANISOPressure.
 *
 */

#ifndef ANISOPRESSURE_H
#define ANISOPRESSURE_H

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <cmath>
#include <stack>
#include <utility>
#include <time.h>
#include <ctime>
/* Opencv includes */
#include <opencv2/opencv.hpp>
using namespace cv;

/**
 * \class ANISOPressure.
 * \brief	A class to store functions to simulate an anisotropic pressure.
 * This class only contains functions modelising
 * different kinds of ANISOPressures that a finger
 * can apply on a plane surface. Every private functions
 * has it arrive set in [0;1] and is meant to
 * be a mask on the original image.
 */
class ANISOPressure{
private:
    /** The height of the image or the mask.
    */
    static uint32_t height_;
    /** The width of the image or the mask.
    */
    static uint32_t width_;
    /** A matrix of elements in [0;1] coding for the anisotropic transformation
    */
    static float **mask;
    /**
    * \brief	A function to check wheter a point(x,y) fits inside the rectangle (0,0,height_,width_)
    *
    * @param  x Abcissa
    *
    * @param  y Ordinate
    *
    * @return  True if it fits, false otherwise
    */
    static bool inImage(int x, int y);
    /**
    * \brief	An implementation of the flood-fill algorithm for binary image (here, for the mask)
    *
    * @param  cx The origin's abcissa
    *
    * @param  cy The origin's ordinate
    */
    static void diffusion(int cx, int cy);
    /**
    * \brief	A function to blur our mask (mean blur with a kernel of size 3x3)
    *
    */
    static void blurMask();
    /**
    * \brief	A function to generate an array of perturbation, i.e. a random smooth curve encoded by dr[]
    *
    * @param  dr Array containing the perturbation
    *
    * @param  N Size of dr
    *
    * @param  irregularity The "granulosity" of the curve
    *
    * @param  max_deformation Amplitude of the deformations
    */
    static void createPerturbation(float* dr, int N, int irregularity, int max_deformation);
    /**
    * \brief	A function to draw our random loop on our mask
    *
    * @param  dr Array containing the perturbation of the ellipse
    *
    * @param  a Half great axis of the ellipse
    *
    * @param  b Half little axis of the ellipse
    *
    * @param  cx Abcissa of the center
    *
    * @param  cy Ordinate of the center
    *
    * @param  N Number of points used to draw the ellipse (have to be high enough to have a closed curve)
    */
    static void drawOnMask(float* dr, float a, float b, int cx, int cy, int N);
    /**
    * \brief	A test function to draw our random loop on our mask
    */
    static void drawOnMask2(float* dr, float a, float b, int cx, int cy, int N, int stroke);
    /* used in shepard interpolation */

    /**
    * \brief	A function to calculate the inverse distance 1/d(x,y)
    *
    * @param  x A real number
    *
    * @param  y A real number
    *
    * @return  1/d(x,y) with d a distance fuction
    */
    static float inverseDistanceWeight(float x, float y);
    /**
    * \brief	Shepard interpolation of the points in dr at the place n*N/I
    *
    * @param  dr The array to interpolate
    *
    * @param  N Size of dr
    *
    * @param  I irregularity, actually quatify the number of fixed random points in the array
    */
    static void shepardInterpolation(float* dr, int N, int I);
    /**
    * \brief	A function to caluate the weighted sum for a point n in dr
    *
    * @param  dr The array of irregularity
    *
    * @param  n Point considered as our current reference
    *
    * @param  N Size of dr
    *
    * @param  I Cf inverseDistanceWeight
    *
    * @return  The wieghted sum
    */
    static float sumWeight(float* dr, int n, int N, int I);
    /**
    * \brief	A function to caluate the weighted sum for a point n in dr
    *
    * @param  dr The array of irregularity
    *
    * @param  n Point considered as our current reference
    *
    * @param  N Size of dr
    *
    * @param  I Cf inverseDistanceWeight
    *
    * @return  The wieghted sum
    */
    static float weightedPoints(float* dr, int n, int N, int I);

 public:
    /**
    * \brief	A function to use an image as a mask
    *
    * @param  path The path to the image
    *
    * @return  1 if success, 0 otherwise
    */
    static int maskFromBitmap(String path);
    /**
    * \brief	A function to automatically generate a mask
    *
    * @param  image The image on which we will work
    *
    * @param  intensity Intensity of the pressure (in [0;1])
    *
    * @param  cx Center of maximal pressure (abcissa)
    *
    * @param  cy Center of maximal pressure (ordinate)
    *
    * @return  1 if success, 0 otherwise
    */
    static int generateMask(Mat image, float intensity, int cx, int cy);
    /**
    * \brief	A function which applies the given transformation to img (in place).
    *
    * @param  img The image we want to transform
    *
    * @return  1 if success 0 otherwise
    */
    static int apply(Mat img);
    /**
    * \brief	A function which gives a grayscale representation of the
    * transformation where white = 0 and black = 1.
    *
    * @param  img The image to apply the transform on
    *
    * @return  1 if success, 0 otherwise
    */
    static int visualize(Mat img);
};

#endif
