/**
 *  @file	starter4.cpp
 *  @brief	Implement some methods of the class Image suach as binarisation, dilation, erosion, etc....
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <vector>
#include <math.h>
#include <utility> //for std::pair

#include "image.h"
#include "structuring_element.h"
#include "functions.h"


void Image::binarize_with_threshold(Mat& mat, float threshold){
  for(int x = 0; x < width; x++)
    for(int y = 0; y < height; y++){
      mat.at<uchar>(y, x) = (matrix.at<uchar>(y, x) > threshold) ? 255 : 0;
    }
}

Image Image::binarize_with_threshold(float threshold){
  Mat mat = matrix.clone();
  binarize_with_threshold(mat, threshold);
  return  Image(mat.clone());
}

Image Image::binarize(){
  Mat tmp = matrix.clone();
  float max_accuracy = 0, max_threshold;
  float current_accuracy;
  for(float threshold = 0.; threshold <= 255.; threshold++){
    binarize_with_threshold(tmp, threshold);
    current_accuracy = this->compare_image(tmp);
    if(current_accuracy > max_accuracy){
      max_accuracy = current_accuracy;
      max_threshold = threshold;
    }
  }
  binarize_with_threshold(tmp, max_threshold);

  Image result = Image(tmp);
  return result;
}


Image Image::dilate(Structure str, int size) {
  Mat tmp = matrix.clone();
  StructuringElement structure = StructuringElement(str, size);
  for(size_t y = 0; y < height ; y++)
    for(size_t x = 0; x < width ; x++)
      if (structure.overlap(matrix, x, y))
        fill(tmp, structure, x, y, 0);

  return tmp;
}

Image Image::erose(Structure str, int size) {
  Mat tmp = matrix.clone();
  StructuringElement structure = StructuringElement(str, size);
  for(size_t y = 0; y < height ; y++)
    for(size_t x = 0; x < width ; x++)
      if (structure.underlap(matrix, x, y))
        fill(tmp, structure, x, y, 255);

  return tmp;
}

Image Image::hit_or_miss(){
  //Mat tmp = matrix.clone();
  Mat tmp = Mat(height, width, CV_8UC1, Scalar(255));
  CompositeElement elt;


  uchar elt2[3][3] = {{255, 0, 255}, {255, 0, 0}, {255, 255, 255}};
  uchar elt1[3][3] = {{255, 255, 255},{0, 255, 255}, {0, 0, 255}};

  elt.first = StructuringElement(Mat(3, 3, CV_8UC1, &elt1));
  elt.second = StructuringElement(Mat(3, 3, CV_8UC1, &elt2));

  for(size_t y = 0; y < height ; y++)
    for(size_t x = 0; x < width ; x++)
      if (elt.first.fits_inside(matrix, x, y)
      && elt.second.fits_outside(matrix, x, y))
        if(Isinside_(tmp, x+1, y+1))
          tmp.at<uchar>(y+1, x+1) = 0;

  return tmp;
}

Mat Image::hit_or_miss(CompositeElement elt){
  //Mat tmp = matrix.clone();
  Mat tmp = Mat(height, width, CV_8UC1, Scalar(255));

  for(size_t y = 0; y < height ; y++)
    for(size_t x = 0; x < width ; x++)
      if (elt.first.fits_inside(matrix, x, y)
      && elt.second.fits_outside(matrix, x, y))
        if(Isinside_(tmp, x+1, y+1))
          tmp.at<uchar>(y+1, x+1) = 0;

  return tmp; //WTF POURQUOI CA MARCHE ??? TODO TODO TODO
}

Mat set_difference(Mat A, Mat B){
  Mat A_minus_B =  A.clone();
  int colorA, colorB;

  for(int i = 0; i < A.rows; i++)
    for(int j = 0; j < A.cols; j++){
      colorA = A.at<uchar>(i, j);
      colorB = B.at<uchar>(i, j);

      if(colorA == 255){
        A_minus_B.at<uchar>(i, j) = 255;
      }else{
        if(colorA == 0 && colorB == 0)
          A_minus_B.at<uchar>(i, j) = 255;
        if(colorA == 0 && colorB == 255)
          A_minus_B.at<uchar>(i, j) = 0;
      }
    }
    return A_minus_B;
}

Mat set_union(Mat A, Mat B){
  Mat A_union_B =  A.clone(); //Already contains A
  int colorB;

  for(int i = 0; i < A.rows; i++)
    for(int j = 0; j < A.cols; j++){
      colorB = B.at<uchar>(i, j);
      if(colorB == 0)
        A_union_B.at<uchar>(i, j) = 0;
    }
    return A_union_B;
}

Mat set_intersection(Mat A, Mat B){
  Mat A_union_B =  A.clone(); //Already contains A
  int colorA, colorB;

  for(int i = 0; i < A.rows; i++)
    for(int j = 0; j < A.cols; j++){
      colorA = A.at<uchar>(i, j);
      colorB = B.at<uchar>(i, j);
      if(colorB == 0 && colorA == 0)
        A_union_B.at<uchar>(i, j) = 0;
      else
        A_union_B.at<uchar>(i, j) = 255;
    }
    return A_union_B;
}

Image Image::squeletonize(){
  Mat squeletonized = matrix.clone();
  Mat previous = squeletonized;
  Mat hit_or_missed;
  CompositeElement elt[8]; //The eight structuring (Golay's Li) elements

  create_Golay_L(elt);

  do{
    previous = squeletonized;
    for(int i = 0; i < 8; i++){
      hit_or_missed = Image(squeletonized).hit_or_miss(elt[i]);
      squeletonized = set_difference(squeletonized, hit_or_missed);
    }
  }while(!areEqual(previous,squeletonized));

  return squeletonized;
}

Image Image::prune(){
  Mat pruned = matrix.clone();
  Mat hit_or_missed;
  CompositeElement elt[8]; //The eight structuring (pruning) elements

  create_pruning_elements(elt);

  for(int i = 0; i < 8; i++){
    hit_or_missed = Image(pruned).hit_or_miss(elt[i]);
    pruned = set_difference(pruned, hit_or_missed);
  }

  return pruned;
}

Image Image::prune(int n){
  Image pruned = Image(matrix.clone()); //Matrix is supposed to be a skeleton
  Mat endpoints(matrix.rows, matrix.cols, CV_8UC1, Scalar(255)); //matrix of endpoints (should describe an empty set)
  Mat hit_or_missed;
  CompositeElement elt[8]; //The eight structuring (pruning) elements

  create_pruning_elements(elt); //Are also able to detect endpoints

  //We prune the image
  for(int i = 0; i < n; i++)
    pruned = pruned.prune();

  //Post treatment : we first find the new endpoint, we dilate it and we interset it with the first skeleton
  for(int i = 0; i < 8; i++){
    hit_or_missed = Image(pruned).hit_or_miss(elt[i]);
    endpoints = set_union(endpoints, hit_or_missed);
  }

  Image endpoints_img(endpoints);

  for(int i = 0; i < n; i++)
    endpoints_img = endpoints_img.dilate(SQUARE, 3);

  Mat extended_endpoints = set_intersection(endpoints_img.matrix, matrix);
  Mat corrected_prunning = set_union(pruned.matrix, extended_endpoints);

  return corrected_prunning;
}
