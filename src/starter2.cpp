/**
 *  @file	starter2.cpp
 *  @brief	Implement some methods of the class Image.
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <vector>
#include <math.h>

# include "image.h"
# include "pixel.h"
#include "functions.h"
using std::cout;
using std::cin;
using std::endl;
using std::cerr;


Mat Image::rotated_image(float angle) {
  Mat rotated_matrix = Mat(height, width, matrix.type(), Scalar(255));
  vector<vector<Pixel> > pixels(height);
  vector<Pixel> pixs;
  for (size_t i = 0; i < height; i++) {
    for (size_t j = 0; j < width; j++) {
      Pixel pix;
      pixs.push_back(pix);
    }
    pixels[i] = pixs;
    pixs.clear();
  }
  vector<float> center{(float)width/2, (float)height/2};
  vector<vector<float> > neighbours;
  vector<float> new_coord;
  for(size_t y = 0; y < height+1 ; y++) {
    for(size_t x = 0; x < width ; x++){
        new_coord = rotate(x, y, angle, center);
        if (!Areinteger(new_coord)) {
          neighbours = coord_neighbours(new_coord);
          for (auto n : neighbours) {
            if (Isinside(matrix, n)) {
              float coef = 1 / distance(n[0], n[1], new_coord);
              pixels[n[1]][n[0]].updateIntensity(matrix.at<uchar>(y, x)*coef);
              pixels[n[1]][n[0]].updateCoefs(coef);
            }
          }
        } else {
          if (Isinside(matrix, new_coord)) {
            pixels[new_coord[1]][new_coord[0]].updateIntensity(matrix.at<uchar>(y, x));
            pixels[new_coord[1]][new_coord[0]].updateCoefs(1);
          }
        }
        neighbours.clear();
      new_coord.clear();
    }
  }

  for(size_t y = 0; y < height ; y++) {
    for(size_t x = 0; x < width ; x++){
      rotated_matrix.at<uchar>(y, x) = pixels[y][x].getfinalIntensity();
    }
  }
  return rotated_matrix;
}

Mat Image::translated_image(vector<float> T) {
  Mat translated_matrix = Mat(height, width, matrix.type(), Scalar(255));
  vector<float> new_coord;
  for(size_t y = 0; y < height ; y++) {
    for(size_t x = 0; x < width ; x++){
      new_coord = translate(x, y, T);
      if (Isinside(matrix, new_coord)) {
        translated_matrix.at<uchar>(new_coord[1], new_coord[0]) = matrix.at<uchar>(y, x);
      }
      new_coord.clear();
    }
  }
  return translated_matrix;
}

float Image::compare_image(Mat matrix1) {
  float accuracy = 0;
  for(size_t y = 0; y < height ; y++) {
    for(size_t x = 0; x < width ; x++){
      float diff = abs(matrix.at<uchar>(y, x) - matrix1.at<uchar>(y, x));
      accuracy += 1 - diff/255;
    }
  }
  accuracy /= (height*width);
  accuracy *= 100;
  return accuracy;
}

double Image::compare_double_image(Mat matrix1) {
  double accuracy = 0;
  for(size_t y = 0; y < height ; y++) {
    for(size_t x = 0; x < width ; x++){
      double diff = fabs(matrix.at<double>(y, x) - matrix1.at<double>(y, x));
      accuracy += 1 - diff;
    }
  }
  accuracy /= (height*width);
  accuracy *= 100;
  return accuracy;
}
