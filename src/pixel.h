/**
 *  \file	pixel.h
 *  \brief	Implement a class Pixel.
 *
 */

#ifndef PIXEL_H
#define PIXEL_H

/**
 * \class Pixel
 *
 * \brief	A class to store a pixel.
 *
 */
class Pixel{
public:
  /**
   * \brief	A constructor of the class Pixel.
   *
   * @return  A white pixel of value 255.
   */
  Pixel();
  /**
   * \brief	A method to obtain the final intensity of a pixel.
   *
   * @return  the final intensity.
   */
  int getfinalIntensity();
  /**
   * \brief	A method to update the intensity of the pixel
   *
   * @param new_intensity The partial intensity that will be added.
   */
  void updateIntensity(float new_intensity);
  /**
   * \brief	A method to check if the pixel has already been modified.
   *
   * @return  true if the pixel has already been updated, false otherwise.
   */
  bool isModified();
  /**
   * \brief	A method to update the value of the weight of the pixel.
   *
   * @param new_coef The partial weight that will be added.
   */
  void updateCoefs(float new_coef);
private:
  /** The sum of the partial intensities of the pixel.
  */
  float intensity_ = 255;
  /** The state of the pixel : true means that the pixel had already been modified.
  */
  bool isModified_ = false;
  /** The weights of the partial intensities.
  */
  float coefs_ = 0;
};

#endif
