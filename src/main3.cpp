/**
 *  @file	main3.cpp
 *  @brief	Implement some methods of the class Image to blur a image with a variant kernel.
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <cmath>
#include <algorithm>

#include "pixel.h"
#include "image.h"

void Image::setEnergy(double value){
    matrix = value/sum(matrix)[0]*matrix;
}

double Image::distance_max(unsigned int c_x, unsigned int c_y){
    double X = (double) c_x;
    double Y = (double) c_y;
    double alldistance[] = {sqrt(Y*Y+X*X), \
            sqrt((width-X)*(width-X) + (height-Y)*(height-Y)), \
            sqrt(X*X + (height-Y)*(height-Y)), \
            sqrt((width-X)*(width-X) + Y*Y)};
    return *(std::max_element(alldistance,alldistance+4));
}

double Image::linearEnergy(double distance, double dmax){
    return 1 - distance/dmax;
}

double Image::quadraticEnergy(double distance, double dmax){
    return std::max(double(0),1 - (distance/dmax)*(distance/dmax)*2);
}

double Image::gaussEnergy(double distance, double dmax){
    double sigma = dmax/3.5;
    double pi = 3.141592654;
    return std::exp(-(distance*distance)/(2*sigma*sigma));
}

Image Image::createKernel(double coef, int KERNEL_TYPE, Size size){
    if (coef == 0){
        return createKernel(1,ID,size);
    }
    double dkmax = sqrt(size.height*size.height/4 + size.width*size.width/4);
    switch(KERNEL_TYPE)
    {
        case ID : {
            Mat kernel = Mat::zeros(size,CV_64F);
            kernel.at<double>(size.height/2,size.width/2) = 1;
            return Image(kernel);
            break;
        }
        case MEDIUM : {
            Mat kernel = Mat::ones(size,CV_64F)/(size.height*size.width);
            return Image(kernel);
            break;
        }
        case TRIANGLE : {
            Mat kernel(size,CV_64F);
            double d;
            int midx = size.width/2;
            int midy = size.height/2;
            for (unsigned int y = 0; y<size.height;y++) {
                for (unsigned int x = 0; x<size.width;x++){
                    d = sqrt((x-midx)*(x-midx)+(y-midy)*(y-midy))/dkmax;
                    kernel.at<double>(y,x) = std::max((double)0,1-d/coef);
                }
            }
            return Image(kernel);
            break;
        }
        case PARABOLA : {
            Mat kernel(size,CV_64F);
            double d;
            int midx = size.width/2;
            int midy = size.height/2;
            for (unsigned int y = 0; y<size.height;y++) {
                for (unsigned int x = 0; x<size.width;x++){
                    d = sqrt((x-midx)*(x-midx)+(y-midy)*(y-midy))/dkmax;
                    kernel.at<double>(y,x) = std::max((double)0,1-d*d/coef);
                }
            }
            return Image(kernel);
            break;
        }
        case K_GAUSS : {
            Mat kernel(size,CV_64F);
            double d;
            int midx = size.width/2;
            int midy = size.height/2;
            for (unsigned int y = 0; y<size.height;y++) {
                for (unsigned int x = 0; x<size.width;x++){
                    d = sqrt((x-midx)*(x-midx)+(y-midy)*(y-midy))/dkmax;
                    double sigma = coef;
                    double pi = 3.1415927;
                    kernel.at<double>(y,x) = std::exp(-(d*d)/(2*sigma*sigma))/(sigma*sqrt(2*pi));
                }
            }
            return Image(kernel);
            break;
        }
    }


}

Image Image::special_convolution(int ENERGY_FUNCTION, unsigned int c_x, unsigned int c_y, int KERNEL_TYPE, Size kernelSize, int borderType, int value_ct){
    int ihe = 0;
    matrix = 255-matrix;
    Mat ans(height, width, CV_64F);
    double dmax = this->distance_max(c_x,c_y);
    unsigned int kh = kernelSize.height;
    unsigned int kw = kernelSize.width;
    int ind_x[kw];
    int ind_y[kh];
    for (unsigned int y = 0 ;y<kh;y++){
        ind_y[y] = y-kh/2;
    }
    for (unsigned int x = 0 ;x<kw;x++){
        ind_x[x] = x-kw/2;
    }
    Mat mat_border = Mat(height+kh, width+kw, matrix.type(), Scalar::all(0));
    copyMakeBorder(this->get_matrix(), mat_border, kh/2, kh-kh/2, kw/2, kw - kw/2, borderType, value_ct);
    Image img_border = Image(mat_border).cast_into_float();
    for (unsigned int y = 0; y<height;y++) {
        ihe = 0;
        for (unsigned int x = 0; x<width;x++){
            double sum = 0;
            double distance = sqrt((c_x-x)*(c_x-x) + (c_y-y)*(c_y-y));
            Image img_kernel = createKernel(distance/dmax,KERNEL_TYPE,kernelSize);
            switch(ENERGY_FUNCTION)
            {
                case LINEAR : {
                    img_kernel.setEnergy(linearEnergy(distance,dmax));
                    break;
                }
                case QUADRATIC : {
                    img_kernel.setEnergy(quadraticEnergy(distance,dmax));
                    break;
                }
                case E_GAUSS : {
                    img_kernel.setEnergy(gaussEnergy(distance,dmax));
                    break;
                }
            }
            for (unsigned int ky=0; ky<kh; ky++){
                for (unsigned int kx=0; kx<kw;kx++){
                    unsigned int abs_x = x + ind_x[kx];
                    unsigned int ord_y = y + ind_y[ky];
                    sum += img_kernel.get_float_pixel(kx, ky)*img_border.get_float_pixel(abs_x + kw/2, ord_y + kh/2);
                }
            }
            double a = cv::sum(img_kernel.get_matrix())[0];
            ans.at<double>(y,x) = sum;
        }
    }
    ans = 1 - ans;
    matrix = 255 - matrix;
    Image filter_image = Image(ans);
    return filter_image.cast_into_int();
}

Mat Image::reverseKernel(Mat kernel){
    Mat revmat = - kernel;
    int h,w;
    h = kernel.size().height;
    w = kernel.size().width;
    revmat.at<double>(h/2,w/2) = 2 + revmat.at<double>(h/2,w/2);
    return revmat;
}
