/**
 *  \file	isopressure.h
 *  \brief	Implement a class ISOPressure.
 *
 */

#ifndef ISOPRESSURE_H
#define ISOPRESSURE_H

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <cmath>
/* Opencv includes */
#include <opencv2/opencv.hpp>
using namespace cv;

/**
 * An enumeration for several types of pressure functions.
 */
enum PType{
  GAUSSIAN,
  SPHERICAL,
  PARABOLIC,
  PHYSICAL,
  GAMMA02,
  POWER
};

/**
 * \class ISOPressure.
 * \brief	A class to store functions to simulate an isotropic pressure.
 * This class only contains functions modelising
 * different kinds of ISOPressures that a finger
 * can apply on a plane surface. Every private functions
 * has it arrive set in [0;1] and is meant to
 * be a mask on the original image.
 */
class ISOPressure{
 private:
   /** The height of the image or the mask.
   */
   static uint32_t height_;
   /** The width of the image or the mask.
   */
   static uint32_t width_;
    /** Abscissa of the center of the pressure
    */
    static uint32_t cx_;
    /** Ordinate of the center of the pressure
    */
    static uint32_t cy_;
    /** The intensity of the pressure (in [0;1])
    */
    static float intensity_;
    /* Our different transformations */

    /**
    * \brief	A function to calculate the intensity of the pressure in x, y for a gaussian function
    *
    * @param  x Abscissa
    *
    * @param  y Ordinate
    *
    * @return  Pressure intensity (in [0;1])
    */
    static float gaussian_(uint32_t x, uint32_t y);
    /**
    * \brief	A function to calculate the intensity of the pressure in x, y for a spherical function
    *
    * @param  x Abscissa
    *
    * @param  y Ordinate
    *
    * @return  Pressure intensity (in [0;1])
    */
    static float spherical_(uint32_t x, uint32_t y);
    /**
    * \brief	A function to calculate the intensity of the pressure in x, y for a parabolic function
    *
    * @param  x Abscissa
    *
    * @param  y Ordinate
    *
    * @return  Pressure intensity (in [0;1])
    */
    static float parabolic_(uint32_t x, uint32_t y);
    /**
    * \brief	A function to calculate the intensity of the pressure in x, y for a physical modelisation of the finger
    *
    * @param  x Abscissa
    *
    * @param  y Ordinate
    *
    * @return  Pressure intensity (in [0;1])
    */
    static float physical_(uint32_t x, uint32_t y);
    /**
    * \brief	A function to calculate the intensity of the pressure in x, y for the "Gamma02" function
    *
    * @param  x Abscissa
    *
    * @param  y Ordinate
    *
    * @return  Pressure intensity (in [0;1])
    */
    static float gamma02_(uint32_t x, uint32_t y);
    /**
    * \brief	A function to calculate the intensity of the pressure in x, y for an exponential function
    *
    * @param  x Abscissa
    *
    * @param  y Ordinate
    *
    * @return  Pressure intensity (in [0;1])
    */
    static float power_(uint32_t x, uint32_t y);

 public:
    /**
    * \brief	A function to apply the given transformation to img (in place).
    *
    * @param  img The image which we want to transform
    *
    * @param  intensity intensity of the pressure (in [0;1])
    *
    * @param  cx Center of maximal pressure (abcissa)
    *
    * @param  cy Center of maximal pressure (ordinate)
    *
    * @param  type Type of transformation to apply (GAUSSIAN, SPHERICAL, PHYSICAL, GAMMA02 or POWER)
    *
    * @return  1 if success, 0 otherwise
    */
    static int apply(Mat img, float intensity,
                    uint32_t cx, uint32_t cy,
                    PType type);
    /**
    * \brief	A function to visualize a transforation in grayscale
    *
    * @param  img The image which we want to use to print the transformation
    *
    * @param  intensity intensity of the pressure (in [0;1])
    *
    * @param  cx Center of maximal pressure (abcissa)
    *
    * @param  cy Center of maximal pressure (ordinate)
    *
    * @param  type Type of transformation to apply (GAUSSIAN, SPHERICAL, PHYSICAL, GAMMA02 or POWER)
    *
    * @return  1 if success, 0 otherwise
    */
    static int visualize(Mat img, float intensity,
                   uint32_t cx, uint32_t cy,
                   PType type);
};

#endif
