/**
 *  @file	main2.cpp
 *  @brief	Implement some methods of the class Image.
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <vector>
#include <math.h>
#include <cmath>

#include "pixel.h"
#include "image.h"
#include "functions.h"

Mat Image::squeeze_image(vector<float> center) {
  Mat squeezed_matrix = Mat(height, width, matrix.type(), Scalar(255));
  vector<vector<Pixel> > pixels(height);
  vector<Pixel> pixs;
  for (size_t i = 0; i < height; i++) {
    for (size_t j = 0; j < width; j++) {
      Pixel pix;
      pixs.push_back(pix);
    }
    pixels[i] = pixs;
    pixs.clear();
  }
  vector<vector<float> > neighbours;
  vector<float> new_coord;
  float dist;
  for(size_t y = 0; y < height ; y++) {
    for(size_t x = 0; x < width ; x++){
      dist = distance(x, y, center);
      if ((dist < 50) && (matrix.at<uchar>(y, x) > 210 || matrix.at<uchar>(y, x) < 100) ) {
          new_coord = warp(x, y, center);
          if (!Areinteger(new_coord)) {
            neighbours = coord_neighbours(new_coord);
            for (auto n : neighbours) {
              if (Isinside(matrix, n)) {
                float coef = 1 / distance(n[0], n[1], new_coord);
                pixels[n[1]][n[0]].updateIntensity(matrix.at<uchar>(y, x)*coef);
                pixels[n[1]][n[0]].updateCoefs(coef);
              }
            }
          } else {
            if (Isinside(matrix, new_coord)) {
              pixels[new_coord[1]][new_coord[0]].updateIntensity(matrix.at<uchar>(y, x));
              pixels[new_coord[1]][new_coord[0]].updateCoefs(1);
            }
          }
          neighbours.clear();
        new_coord.clear();
      } else {
        pixels[y][x].updateIntensity(matrix.at<uchar>(y, x));
        pixels[y][x].updateCoefs(1);
      }
    }
  }

  for(size_t y = 0; y < height ; y++) {
    for(size_t x = 0; x < width ; x++){
      squeezed_matrix.at<uchar>(y, x) = pixels[y][x].getfinalIntensity();
    }
  }
  return squeezed_matrix;
}

Mat Image::locally_rotate_image(vector<float> center, float strength) {
  Mat locally_rotated_matrix = Mat(height, width, matrix.type(), Scalar(255));
  vector<vector<Pixel> > pixels(height);
  vector<Pixel> pixs;
  for (size_t i = 0; i < height; i++) {
    for (size_t j = 0; j < width; j++) {
      Pixel pix;
      pixs.push_back(pix);
    }
    pixels[i] = pixs;
    pixs.clear();
  }
  vector<vector<float> > neighbours;
  vector<float> new_coord;
  float dist;
  for(size_t y = 0; y < height ; y++) {
    for(size_t x = 0; x < width ; x++){
      dist = distance(x, y, center);
        new_coord = rotate(x, y, relative_angle(dist, strength), center);
        if (!Areinteger(new_coord)) {
          neighbours = coord_neighbours(new_coord);
          for (auto n : neighbours) {
            if (Isinside(matrix, n)) {
              float coef = 1 / distance(n[0], n[1], new_coord);
              pixels[n[1]][n[0]].updateIntensity(matrix.at<uchar>(y, x)*coef);
              pixels[n[1]][n[0]].updateCoefs(coef);
            }
          }
        } else {
          if (Isinside(matrix, new_coord)) {
            pixels[new_coord[1]][new_coord[0]].updateIntensity(matrix.at<uchar>(y, x));
            pixels[new_coord[1]][new_coord[0]].updateCoefs(1);
          }
        }
        neighbours.clear();
      new_coord.clear();
    }
  }

  for(size_t y = 0; y < height ; y++) {
    for(size_t x = 0; x < width ; x++){
      locally_rotated_matrix.at<uchar>(y, x) = pixels[y][x].getfinalIntensity();
    }
  }
  return locally_rotated_matrix;
}

Mat Image::locally_translate_image(vector<float> center, float strength) {
  Mat locally_translated_matrix = Mat(height, width, matrix.type(), Scalar(255));
  vector<vector<Pixel> > pixels(height);
  vector<Pixel> pixs;
  for (size_t i = 0; i < height; i++) {
    for (size_t j = 0; j < width; j++) {
      Pixel pix;
      pixs.push_back(pix);
    }
    pixels[i] = pixs;
    pixs.clear();
  }
  vector<vector<float> > neighbours;
  vector<float> new_coord;
  float dist;

  for(size_t y = 0; y < height ; y++) {
    for(size_t x = 0; x < width ; x++){
        dist = distance(x, y, center);
        new_coord = translate(x, y, relative_translation_vector(dist, strength));
        if (!Areinteger(new_coord)) {
          neighbours = coord_neighbours(new_coord);
          for (auto n : neighbours) {
            if (Isinside(matrix, n)) {
              float coef = 1 / distance(n[0], n[1], new_coord);
              pixels[n[1]][n[0]].updateIntensity(matrix.at<uchar>(y, x)*coef);
              pixels[n[1]][n[0]].updateCoefs(coef);
            }
          }
        } else {
          if (Isinside(matrix, new_coord)) {
            pixels[new_coord[1]][new_coord[0]].updateIntensity(matrix.at<uchar>(y, x));
            pixels[new_coord[1]][new_coord[0]].updateCoefs(1);
          }
        }
        neighbours.clear();
      new_coord.clear();
    }
  }

  for(size_t y = 0; y < height ; y++) {
    for(size_t x = 0; x < width ; x++){
      locally_translated_matrix.at<uchar>(y, x) = pixels[y][x].getfinalIntensity();
    }
  }
  return locally_translated_matrix;
}

Mat Image::darken_image(vector<float> center) {
    vector<vector<float> > squar = square(center);
    float dist;
    float intensity;
    float new_intensity;
    vector<float> new_coord;
    Mat new_image = matrix.clone();
    for(size_t y = 0; y < height ; y++) {
      for(size_t x = 0; x < width ; x++){
        dist = distance(x, y, center);
        intensity = new_image.at<uchar>(y, x);
        if (dist < 120) {
          new_intensity = darken_intensity(intensity, dist);
          new_image.at<uchar>(y, x) = (new_intensity >= 0)?new_intensity:0;
        }
      }
    }
    //local interpolation around the center that we now don't really need
    /*
    for(size_t x = squar[0][0]; x < squar[1][0] ; x++){
      for(size_t y = squar[0][1]; y < squar[2][1] ; y++) {
        if (new_image.at<uchar>(y, x) > 200) {
          new_image.at<uchar>(y, x) = (new_image.at<uchar>(y, x) + new_image.at<uchar>(y+1, x))/2;
        }

      }
    }
    for(size_t x = squar[0][0]; x < squar[1][0] ; x++){
      for(size_t y = squar[0][1]; y < squar[2][1] ; y++) {
        if (new_image.at<uchar>(y, x) > 200) {
          new_image.at<uchar>(y, x) = (new_image.at<uchar>(y, x) + new_image.at<uchar>(y, x+1))/2;
        }
      }
    }*/
    return new_image;
}

Mat Image::warp_image(vector<float> center, float strength) {
  Mat new_image = this->squeeze_image(center);
  Image modif = Image(new_image);

  new_image = modif.darken_image(center);
  Image modif1 = Image(new_image);

  Mat new1_image = modif1.locally_rotate_image(center, strength);
  Image modif2 = Image(new1_image);

  Mat new2_image = modif2.locally_translate_image(center, strength);

  return new2_image;
}
