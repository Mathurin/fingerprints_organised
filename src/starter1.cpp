/**
 *  @file	starter1.cpp
 *  @brief	Implement some basic methods of the class Image as loading,
 * displaying, ...and  the symmetry transformations.
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <vector>
#include <math.h>

# include "image.h"
# include "pixel.h"


using namespace cv;
using std::cout;
using std::cin;
using std::endl;
using std::cerr;

Image::Image(Mat m){
    matrix = m;
    width = matrix.cols;
    height = matrix.rows ;
}

Image::Image(const string& filename){
    matrix = imread( filename, CV_LOAD_IMAGE_GRAYSCALE );
    width = matrix.size().width;
    height = matrix.size().height;
}

Image::Image(unsigned int w, unsigned int h){
    width = w;
    height = h;
    matrix = Mat(h, w, CV_8UC1 ,Scalar(255));
}

Mat Image::get_matrix()const{
    return matrix;
}

double Image::get_float_pixel(unsigned int x, unsigned int y)const{
    switch (matrix.type())
    {
        case 4:
        case 5:
            return (float)matrix.at<float>(y, x);
            break;
        case 6:
            return (double)matrix.at<double>(y, x);
            break;
        default:
  cerr << "Problem of type (float), not recognise:  " << matrix.type() << endl;;
    }
}

unsigned int  Image::get_int_pixel(unsigned int x, unsigned int y)const{
    switch (matrix.type())
    {
        case 0:
        case 1:
            return (int)matrix.at<uint8_t>(y, x);
            break;
        case 2:
        case 3:
            return (int)matrix.at<uint16_t>(y, x);
            break;
        default:
            cerr << "Problem of type (int), not recognise:  " << matrix.type() << endl;;
    }
}
void Image::display_image(const string& title, unsigned int time){
    namedWindow(title, WINDOW_AUTOSIZE );
    imshow(title, matrix);
    waitKey(time);
}

Image Image::diff_contraste(Mat m){
    Mat result;
    normalize(abs(matrix-m), result, 255, 0,NORM_MINMAX);
    return Image(result);
}

void Image::display_pixels(){
    Vec2f intensity;
    for (unsigned int i=0; i<width; i++){
        for (unsigned int j=0; j<height; j++){
            switch (matrix.type())
            {
                case 0:
                    cout << "column:  " << i << "  , row:  " << j << "  , value:  "
                    << (int)matrix.at<uint8_t>(j, i) << endl;
                    break;
                case 5:
                    cout << "column:  " << i << "  , row:  " << j << "  , value:  "
                    << (float)matrix.at<float>(j, i) << endl;
                    break;
                case 6:
                    cout << "column:  " << i << "  , row:  " << j << "  , value:  "
                    << (double)matrix.at<double>(j, i) << endl;
                    break;
                case 13:
                    intensity = matrix.at<Vec2f>(j, i);
                    cout << "column:  " << i << "  , row:  " << j << "  , value:  " <<intensity << endl;
                    break;
                default:
                    cerr << "Can't find the type for the value" << matrix.type() << endl;
            }
        }
    }
}

void Image::display_informations(){
    cout << "Some informations about the image: " << endl;
    cout << "Depth: " << matrix.depth() << endl;
    cout << "Type of values stored: " << matrix.type() << endl;
    cout << "Size: " << matrix.size()  << endl;
    cout << "Height: " << matrix.size().height  << endl;
    cout << "Width: " << matrix.size().width  << endl;
}

void Image::save_image(const string& directory){
    imwrite(directory, matrix);
}

Image Image::draw_rectangle(unsigned int begin_x, unsigned int end_x, unsigned int begin_y, unsigned int end_y, unsigned int color){
    Mat m;
    matrix.copyTo(m);
    Image m_bis = Image(m);
    if ((begin_x > end_x) || (end_x > this->width) || (begin_y > end_y) || (end_y > this->height)){
        cerr << "Error of input !" << endl;
    }
    for (unsigned int i=begin_x; i<end_x; i++){
        for (unsigned int j=begin_y; j<end_y; j++){
            m_bis.matrix.at<uchar>(j, i) = color;
        }
    }
    return m_bis;
}

Image Image::cast_into_float(){
    Mat m_bis = Mat(height, width, CV_64F, Scalar(1.0));
    for (unsigned int i=0; i<width; i++){
        for (unsigned int j=0; j<height; j++){
            m_bis.at<double>(j, i) = matrix.at<uchar>(j, i)/255.0;
        }
    }
    return Image(m_bis);
}

Image Image::cast_into_int(){
    Mat m_bis = Mat(height, width, CV_8U , Scalar(1));
    for (unsigned int i=0; i<width; i++){
        for (unsigned int j=0; j<height; j++){
            m_bis.at<uchar>(j, i) = round(matrix.at<double>(j, i)*255.0);
        }
    }
    return Image(m_bis);
}

Image Image::mult(Image img){
    /* beware : defined on matrix of uint in [0, 255] */
    Image img1_float = this->cast_into_float();
    Image img2_float = img.cast_into_float();
    Mat mat_result = img1_float.matrix * img2_float.matrix;
    Image img_result = Image(mat_result);
    return img_result.cast_into_int();
}

Image Image::symmetry_along_y_axis(){
    Mat J = Mat(width, width, CV_8U, Scalar(0));
    unsigned int j = 0;
    for (unsigned int i=0; i<width; i++){
        j= width-1 - i;
        J.at<uchar>(j, i) = 255;
    }
    Image img = *this;
    Image img2 = Image(J);
    Image result = img.mult(img2);
    return result;
}

Image Image::symmetry_along_x_axis(){
    Mat J = Mat(height, height, CV_8U, Scalar(0));
    unsigned int j = 0;
    for (unsigned int i=0; i<height; i++){
        j= height-1 - i;
        J.at<uchar>(j, i) = 255;
    }
    Image img = *this;
    Image img2 = Image(J);
    Image result = img2.mult(img);
    return result;
}

Image Image::symmetry_along_diag1(){
    Mat m_bis = Mat(height, width,CV_8U, 0);
    transpose(matrix, m_bis);
    return Image(m_bis);
}

Image Image::symmetry_along_diag2(){
    Mat m_bis = Mat(height, width,CV_8U, 0);
    transpose(matrix, m_bis);
    return Image(m_bis).symmetry_along_y_axis();
}
