/**
 *  \file	pixel.h
 *  \brief	 Set of function thought to find the minutiae in the squeleton of a binarized image of fingerprints
 *
 */

#ifndef MINUTIAE_H
#define MINUTIAE_H


#include "image.h"

/**
 * \brief Calculate the number of neighbours (black poitns) of a binarized squeletonized image at point (x,y)
 *
 * @param	image Squeletonized fingerprint
 *
 * @param x Abcissa
 *
 * @param y Ordinate
 *
 * @return Number of neighbours at (x,y)
 */
unsigned int get_neighbours(Image image, int x, int y);

/**
 * \brief Creates the matrix of all CN (crossing numbers) for each pixel
 *
 * @param	squeletonized The squeletonized fingerprint
 *
 * @return 	The CN matrix
 */
Mat create_CN_matrix(Image squeletonized);

/**
 * \brief Display the minutiaes on a fingerprint. In blue, the terminations. In red, branchings
 *
 * @param	fingerprint Image of our fingerprint (cleanest possible). 
 */
void display_minutiae(Image& fingerprint);

#endif
