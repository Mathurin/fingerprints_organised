#include <iostream>
#include <vector>
#include "structuring_element.h"
#include "functions.h"

using namespace std;

StructuringElement::StructuringElement() {
  matrix = Mat(3, 3, CV_8UC1, Scalar(0));
}

StructuringElement::StructuringElement(Structure str, int size) {
  float radius = (float)size/2;
  vector<float> center{radius, radius};
  switch (str) {
    case SQUARE:
      matrix = Mat(size, size, CV_8UC1, Scalar(0));
      break;
    case DIAMOND:
      matrix = Mat(3, 3, CV_8UC1, Scalar(0));
      matrix.at<uchar>(0, 0) = matrix.at<uchar>(2, 2) = 255;
      matrix.at<uchar>(2, 0) = matrix.at<uchar>(0, 2) = 255;
      break;
    case CROSS:
      matrix = Mat(size, size, CV_8UC1, Scalar(255));
      for(size_t j = 0; j < matrix.rows ; j++) {
        matrix.at<uchar>(j, j) = matrix.at<uchar>(j, matrix.rows-j-1) = 0;
      }
      break;
    case CIRCLE:
      matrix = Mat(size, size, CV_8UC1, Scalar(255));
      for(size_t j = 0; j < matrix.rows ; j++)
        for(size_t i = 0; i < matrix.cols ; i++) {
          float dist = distance(i, j, center);
          if (dist <= radius) {
            matrix.at<uchar>(j, i) = 0;
          }
        }
      break;
    default:
      cerr << "Please choose one of these structuring elements:\n";
      cerr << "SQUARE, DIAMOND, CROSS, CIRCLE" << endl;
      break;
  }

}

StructuringElement::StructuringElement(Mat const& str) {
  matrix = str.clone();
}

StructuringElement StructuringElement::rotate(){
  //TODO: Only work if the element is in a square (openCV doesn't implement this ?)
  StructuringElement elt(matrix);

  for(int i = 0; i < matrix.rows; i++)
    for(int j = 0; j < matrix.cols; j++)
      elt.matrix.at<uchar>(i, j) = this->matrix.at<uchar>(j, matrix.rows-1-i);

  return elt;
}

bool StructuringElement::intersect(float x, float y) {
  if (matrix.at<uchar>(y, x) == 0) {
    return true;
  }
  return false;
}

bool StructuringElement::fits_inside(Mat image, int x, int y){
  for(size_t j = 0; j < matrix.rows ; j++)
    for(size_t i = 0; i < matrix.cols ; i++)
      if (Isinside_(image, x+i, y+j))
        if (matrix.at<uchar>(j, i) == 0 && image.at<uchar>(y+j, x+i) != 0)
          return false;
  return true;
}

bool StructuringElement::fits_outside(Mat image, int x, int y){
  for(size_t j = 0; j < matrix.rows ; j++)
    for(size_t i = 0; i < matrix.cols ; i++)
      if (Isinside_(image, x+i, y+j))
        if (matrix.at<uchar>(j, i) == 0 && image.at<uchar>(y+j, x+i) == 0)
          return false;
  return true;
}

bool StructuringElement::overlap(Mat image, float x, float y) {
  for(size_t j = 0; j < matrix.rows ; j++)
    for(size_t i = 0; i < matrix.cols ; i++)
      if (Isinside_(image, x+i, y+j))
        if (image.at<uchar>(y+j, x+i) == 0 && matrix.at<uchar>(j, i) == 0)
          return true;
  return false;
}

int StructuringElement::grayscale_overlap(Mat image, float x, float y) {
  int res = 256;
  for(size_t j = 0; j < matrix.rows ; j++)
    for(size_t i = 0; i < matrix.cols ; i++)
      if (Isinside_(image, x+i, y+j))
        if (/*image.at<uchar>(y+j, x+i) < 240 &&*/ matrix.at<uchar>(j, i) == 0) {
          res = (res > image.at<uchar>(y+j, x+i))?image.at<uchar>(y+j, x+i):res;
        }
  res = (res == 256)?-1:res;
  return res;
}

bool StructuringElement::underlap(Mat image, float x, float y) {
  for(size_t j = 0; j < matrix.rows ; j++)
    for(size_t i = 0; i < matrix.cols ; i++)
      if (Isinside_(image, x+i, y+j))
        if (image.at<uchar>(y+j, x+i) == 255 && matrix.at<uchar>(j, i) == 0)
          return true;
  return false;
}

int StructuringElement::grayscale_underlap(Mat image, float x, float y) {
  int res = -1;
  for(size_t j = 0; j < matrix.rows ; j++)
    for(size_t i = 0; i < matrix.cols ; i++)
      if (Isinside_(image, x+i, y+j))
        if (/*image.at<uchar>(y+j, x+i) >= 20 &&*/ matrix.at<uchar>(j, i) == 0) {
          res = (res < image.at<uchar>(y+j, x+i))?image.at<uchar>(y+j, x+i):res;
        }
  return res;
}

int StructuringElement::rows() {
  return matrix.rows;
}

int StructuringElement::cols() {
  return matrix.cols;
}

/************ CompositeElement **************/
CompositeElement rotate(CompositeElement elt){
  CompositeElement res;
  res.first = elt.first.rotate();
  res.second = elt.second.rotate();
  return res;
}

CompositeElement create_Golay_L1(){
  CompositeElement elt;

  uchar elt2[3][3] = {{0, 0, 0}, {255, 255, 255}, {255, 255, 255}};
  uchar elt1[3][3] = {{255, 255, 255},{255, 0, 255}, {0, 0, 0}};

  elt.first = StructuringElement(Mat(3, 3, CV_8UC1, &elt1));
  elt.second = StructuringElement(Mat(3, 3, CV_8UC1, &elt2));

  return elt;
}

CompositeElement create_Golay_L2(){
  CompositeElement elt;

  uchar elt2[3][3] = {{255, 0, 255}, {255, 255, 0}, {255, 255, 255}};
  uchar elt1[3][3] = {{255, 255, 255},{0, 0, 255}, {0, 0, 255}};

  elt.first = StructuringElement(Mat(3, 3, CV_8UC1, &elt1));
  elt.second = StructuringElement(Mat(3, 3, CV_8UC1, &elt2));

  return elt;
}

void create_Golay_L(CompositeElement* golay_L){
  //Rotate golays elements L1 and L2
  golay_L[0] = create_Golay_L1();
  golay_L[1] = create_Golay_L2();
  golay_L[2] = rotate(golay_L[0]);
  golay_L[3] = rotate(golay_L[1]);
  golay_L[4] = rotate(golay_L[2]);
  golay_L[5] = rotate(golay_L[3]);
  golay_L[6] = rotate(golay_L[4]);
  golay_L[7] = rotate(golay_L[5]);
}

CompositeElement create_pruning_1(){
  CompositeElement elt;

  uchar elt1[3][3] = {{255, 0, 255}, {255, 0, 255}, {255, 255, 255}};
  uchar elt2[3][3] = {{255, 255, 255},{0, 255, 0}, {0, 0, 0}};

  elt.first = StructuringElement(Mat(3, 3, CV_8UC1, &elt1));
  elt.second = StructuringElement(Mat(3, 3, CV_8UC1, &elt2));

  return elt;
}

CompositeElement create_pruning_2(){
  CompositeElement elt;

  uchar elt1[3][3] = {{255, 255, 0}, {255, 0, 255}, {255, 255, 255}};
  uchar elt2[3][3] = {{0, 255, 255},{0, 255, 255}, {0, 0, 0}};

  elt.first = StructuringElement(Mat(3, 3, CV_8UC1, &elt1));
  elt.second = StructuringElement(Mat(3, 3, CV_8UC1, &elt2));

  return elt;
}

void create_pruning_elements(CompositeElement* pruning){
  pruning[0] = create_pruning_1();
  pruning[1] = create_pruning_2();
  pruning[2] = rotate(pruning[0]);
  pruning[3] = rotate(pruning[1]);
  pruning[4] = rotate(pruning[2]);
  pruning[5] = rotate(pruning[3]);
  pruning[6] = rotate(pruning[4]);
  pruning[7] = rotate(pruning[5]);
}
