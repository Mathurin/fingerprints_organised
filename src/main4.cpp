/**
 *  @file	main4.cpp
 *  @brief	Implement the grayscale dilation and erosion.
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <cmath>

# include "image.h"
# include "structuring_element.h"
# include "functions.h"

using namespace std;


Image Image::grayscale_dilate(Structure str, int size) {
  Mat tmp = matrix.clone();
  StructuringElement structure = StructuringElement(str, size);
  for(size_t y = 0; y < height ; y++) {
    for(size_t x = 0; x < width ; x++){
      float maximum_color = (float)structure.grayscale_overlap(matrix, x, y);
      if (maximum_color != -1) {
        fill(tmp, structure, x, y, maximum_color);
      }
    }
  }
  Image result = Image(tmp);
  return result;
}



Image Image::real_grayscale_dilate(vector<float> center) {
  Mat tmp = matrix.clone();
  float dist;
  StructuringElement structure;
  for(size_t y = 0; y < height ; y++) {
    for(size_t x = 0; x < width ; x++){
      dist = distance(x, y, center);
      structure = StructuringElement(dilate_str_coef(dist), dilate_size_coef(dist));
      float maximum_color = (float)structure.grayscale_overlap(matrix, x, y);
      if (maximum_color != -1) {
        fill(tmp, structure, x, y, maximum_color);
      }
    }
  }
  Image result = Image(tmp);
  return result;
}


Image Image::grayscale_erose(Structure str, int size) {
  Mat tmp = matrix.clone();
  StructuringElement structure = StructuringElement(str, size);
  for(size_t y = 0; y < height ; y++) {
    for(size_t x = 0; x < width ; x++){
      float maximum_color = (float)structure.grayscale_underlap(matrix, x, y);
      if (maximum_color != -1) {
        fill(tmp, structure, x, y, maximum_color);
      }
    }
  }
  return tmp;
}


Image Image::real_grayscale_erose(vector<float> center) {
  Mat tmp = matrix.clone();
  float dist;
  StructuringElement structure;
  for(size_t y = 0; y < height ; y++) {
    for(size_t x = 0; x < width ; x++){
      dist = distance(x, y, center);
      structure = StructuringElement(erose_str_coef(dist), erose_size_coef(dist));
      float minimum_color = (float)structure.grayscale_underlap(matrix, x, y);
      if (minimum_color != -1) {
        fill(tmp, structure, x, y, minimum_color);
      }
    }
  }
  return tmp;
}

uint Image::calculate_intensity(uint start_x, uint start_y, uint size_x, uint size_y) {
  uint intensity = 0;
  uint end_x = start_x + size_x;
  uint end_y = start_y + size_y;
  for(uint y = start_y; (y < end_y) && (y < height) ; y++) {
    for(uint x = start_x; (x < end_x) && (x < width) ; x++){
      uint tmp = matrix.at<uchar>(y, x);
      intensity += tmp;
    }
  }

  return intensity;
}

vector<uint> Image::find_center(uint start_x, uint start_y, uint size_x, uint size_y) {
  if (size_x <= 3 || size_y <= 3) {
    vector<uint> center{(uint)round(start_x+size_x/2-1), (uint)round(start_y+size_y/2-1)};
    return center;
  } else {
    uint partition_x = round(size_x/3);
    uint partition_y = round(size_y/3);
    uint final_x = 0;
    uint final_y = 0  ;
    uint intensity = this->calculate_intensity(start_x, start_y, partition_x, partition_y);
    vector<uint> steps_x{partition_x, 2*partition_x, 0, partition_x, 2*partition_x, 0, partition_x, 2*partition_x};
    vector<uint> steps_y{0, 0, partition_y, partition_y, partition_y, 2*partition_y, 2*partition_y, 2*partition_y};
    for(size_t i = 0; i < 8 ; i++) {
        uint new_intensity = this->calculate_intensity(start_x+steps_x[i], start_y+steps_y[i], partition_x, partition_y);
        if (intensity >= new_intensity) {
          final_x = start_x + steps_x[i];
          final_y = start_y + steps_y[i];
          intensity = new_intensity;
        }
    }
    return this->find_center(final_x, final_y, partition_x, partition_y);
  }
}
