/**
 *  @file	main1.cpp
 *  @brief	Implement some methods of the class Image.
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <cmath>

#include "image.h"
#include "pixel.h"
#include "functions.h"
#include "isopressure.h"
#include "anisopressure.h"

int Image::isotropic_pressure(float intensity, int cx, int cy, PType type){
  ISOPressure::apply(matrix, intensity, cx, cy, type);
}

int Image::anisotropic_pressure(float intensity, int cx, int cy){
  ANISOPressure::generateMask(matrix, intensity, cx, cy);
  ANISOPressure::apply(matrix);
}
