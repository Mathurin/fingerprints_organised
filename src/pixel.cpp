/**
 *  @file	pixel.cpp
 *  @brief	Implement some methods of the class Pixel.
 *
 */

#include "pixel.h"
#include "math.h"

Pixel::Pixel() {}

int Pixel::getfinalIntensity() {
  if (isModified_) {
    return (int)round((intensity_/coefs_));
  }
  return 255;
}

void Pixel::updateIntensity(float new_intensity) {
  if (isModified_) {
    intensity_ += new_intensity;
  } else {
    intensity_ = new_intensity;
    isModified_ = true;
  }
  return;
}

void Pixel::updateCoefs(float new_coef) {
  coefs_ += new_coef;
  return;
}

bool Pixel::isModified() {
  return isModified_;
}
