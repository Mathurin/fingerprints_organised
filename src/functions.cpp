/**
 *  @file	functions.cpp
 *  @brief	Implement some useful functions.
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <vector>
#include <math.h>

#include "functions.h"
/**
 * The macro for pi.
 */
#define pi 3.14159265

float distance(float x, float y, vector<float>& center) {
  float dis;
  dis = sqrt(pow(center[1] - y, 2) + pow(center[0] - x, 2));
  return dis;
}

void fill(Mat& tmp, StructuringElement structure, int x, int y, float color) {
  for(size_t j = 0; j < structure.rows() ; j++)
    for(size_t i = 0; i < structure.cols() ; i++)
      if (structure.intersect(i, j) && Isinside_(tmp, x+i, y+j))
        tmp.at<uchar>(y+j, x+i) = color;
}

vector<float> rotate(float x, float y, float angle, vector<float>& center) {
  vector<float> coord;
  float angle_r = pi * angle /180;
  float x_prime = x - center[0];
  float y_prime = y - center[1];
  coord.push_back(x_prime*cos(angle_r) + y_prime*sin(angle_r) + center[0]);
  coord.push_back(-x_prime*sin(angle_r) + y_prime*cos(angle_r) + center[1]);
  //coord.push_back(round(x_prime*cos(angle_r) + y_prime*sin(angle_r) + center[0]));
  //coord.push_back(round(-x_prime*sin(angle_r) + y_prime*cos(angle_r) + center[1]));
  return coord;
}

vector<float> translate(float x, float y, vector<float> T) {
  vector<float> coord;
  coord.push_back(x + T[0]);
  coord.push_back(y + T[1]);
  return coord;
}

vector<vector<float> > coord_neighbours(vector<float> coord) {
  vector<float> coord0{float(floor(coord[0])), float(floor(coord[1]))};
  vector<float> coord1{coord0[0]+1, coord0[1]};
  vector<float> coord2{coord0[0], coord0[1]+1};
  vector<float> coord3{coord0[0]+1, coord0[1]+1};
  vector<vector<float> > coord_neighbours{coord0, coord1, coord2, coord3};
  return coord_neighbours;
}


bool Areinteger(vector<float> coord) {
  if (coord[0] == round(coord[0]) && coord[1] == round(coord[1])) {
    return true;
  }
  return false;
}

bool Isinside(Mat image, vector<float> v) {
  if (v[0] >= 0 && v[0] < image.cols && v[1] >= 0 && v[1] < image.rows) {
    return true;
  }
  return false;
}
bool Isinside_(Mat image, float x, float y) {
  if (x >= 0 && x < image.cols && y >= 0 && y < image.rows) {
    return true;
  }
  return false;
}

vector<vector<float> > square(vector<float> coord) {
  vector<float> coord0{coord[0]-55, coord[1]-55};
  vector<float> coord1{coord[0]+55, coord[1]-55};
  vector<float> coord2{coord[0]-55, coord[1]+55};
  vector<float> coord3{coord[0]+55, coord[1]+55};
  vector<vector<float> > coord_neighbours{coord0, coord1, coord2, coord3};
  return coord_neighbours;
}

float relative_angle(float dist, float strength) {
  return strength*60*exp(-pow(dist, 2)/1000);
}

vector<float> relative_translation_vector(float dist,float strength) {
  vector<float> new_coord;
  float coef = -strength*exp(-pow(dist, 2)/1000);
  new_coord.push_back(coef*40);
  new_coord.push_back(coef*10);
  return new_coord;
}

float darken_intensity(float intensity, float dist) {
  if (intensity < 50) {
    return intensity - 1.3*exp(-(dist/50))* intensity;
  } else if (intensity < 90) {
    return intensity - 0.6*exp(-(dist/50))* intensity;
  } else if (intensity < 150) {
    return intensity - 0.5*exp(-(dist/50))* intensity;
  } else if (intensity < 200) {
    return intensity - 0.1*exp(-(dist/50))* intensity;
  } else if (intensity < 250) {
    return intensity - 0.4*exp(-(dist/20))* intensity;
  }
}


vector<float> warp(float x, float y, vector<float> center) {
  vector<float> coord;
  float dist = distance(x, y, center);
  float epsilonx = -1;
  if (x < center[0]) {
    epsilonx = 1;
  }
  float epsilony = -1;
  if (y < center[1]) {
    epsilony = 1;
  }
  coord.push_back(x + epsilonx *(dist* exp(-(dist/5))));
  coord.push_back(y + epsilony *(dist* exp(-(dist/5))));
  return coord;
}

bool areEqual(const Mat& A, const Mat& B){
  if(A.rows != B.rows || A.cols != B.cols)
    return false;

  for(int i = 0; i < A.rows; i++)
    for(int j = 0; j < A.cols; j++)
      if(A.at<uchar>(i,j) != B.at<uchar>(i,j))
        return false;
  return true;
}

int dilate_size_coef(float dist) {
  if (dist < 50) {
    return 4;
  } else if (dist < 100) {
    return 3;
  } else if (dist < 150) {
    return 2;
  }
  return 1;
}
int erose_size_coef(float dist) {
  if (dist < 50) {
    return 1;
  } else if (dist < 80) {
    return 2;
  } else if (dist < 130) {
    return 3;
  }
  return 4;
}
Structure erose_str_coef(float dist) {
  if (dist < 60) {
    return SQUARE;
  } else if (dist < 150) {
    return CROSS;
  }
  return CIRCLE;
}

Structure dilate_str_coef(float dist) {
  if (dist < 60) {
    return CIRCLE;
  } else if (dist < 150) {
    return SQUARE;
  }
  return CIRCLE;
}
