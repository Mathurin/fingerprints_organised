/**
 *  @file	isopressure.cpp
 *  @brief	Implement some static methods (i.e some functions) of the class ISOPressure.
 *
 */

#include "isopressure.h"

// what is the usefulness of the 3 following lines ?
uint32_t ISOPressure::height_, ISOPressure::width_;
uint32_t ISOPressure::cx_, ISOPressure::cy_;
float ISOPressure::intensity_;

float ISOPressure::power_(uint32_t x, uint32_t y){
/*
 * docstring
 * "On assimile notre doigt a une sphere parfaite"
 * J'adore cette phrase
 */
  float rsq = (x-cx_)*(x-cx_)+(y-cy_)*(y-cy_);
  float Rsq = height_*height_/4;
  float Isq = intensity_*intensity_;

  return (rsq > Rsq*Isq) ? 0 : pow(1-rsq/(Rsq*Isq), 1.-intensity_);
}

float ISOPressure::physical_(uint32_t x, uint32_t y){
/*
 * docstring
 */
  float rsq = (x-cx_)*(x-cx_)+(y-cy_)*(y-cy_); // Note : no need to sqrt, will be squared
  float Rsq = height_*height_/4;
  float Isq = (1-intensity_)*(1-intensity_);

  return (rsq < Rsq*(1-Isq)) ? sqrt(1-rsq/Rsq)-(1-intensity_): 0;
}

float ISOPressure::gaussian_(uint32_t x, uint32_t y){
/*
 * if isotropic we consider that
 * R = min(width,height) = height (convetions)
 */
 float rsq = (x-cx_)*(x-cx_)+(y-cy_)*(y-cy_); // Note : no need to sqrt, will be squared
 float Rsq = height_*height_/4;

  return exp( -(1-1./intensity_)*rsq/Rsq );
}

float ISOPressure::spherical_(uint32_t x, uint32_t y){
/*
 * docstring
 * "On assimile notre doigt a une sphere parfaite"
 * J'adore cette phrase
 */
 float rsq = (x-cx_)*(x-cx_)+(y-cy_)*(y-cy_);
 float Rsq = height_*height_/4;
 float Isq = intensity_*intensity_;

  return (rsq > Rsq*Isq) ? 0 : sqrt(1-rsq/(Rsq*Isq));
}

float ISOPressure::parabolic_(uint32_t x, uint32_t y){
/*
 * docstring
 */
 float rsq = (x-cx_)*(x-cx_)+(y-cy_)*(y-cy_);
 float Rsq = height_*height_/4;
 float Isq = intensity_*intensity_;

  return (rsq > Rsq*Isq) ? 0 : 1-rsq/(Rsq*Isq);
}

float ISOPressure::gamma02_(uint32_t x, uint32_t y){
/*
 * docstring
 */
 float rsq = (x-cx_)*(x-cx_)+(y-cy_)*(y-cy_);
 float Rsq = height_*height_/4;
 float Isq = intensity_*intensity_;

  return (rsq > Rsq*Isq) ? 0 : pow(1-rsq/(Rsq*Isq), 0.2);
}



int ISOPressure::apply(Mat img, float intensity,
                    uint32_t cx, uint32_t cy,
                    PType type){

  /* We affect the values to our member variables */
  height_ = img.size().height;
  width_  = img.size().width;
  cx_ = cx;
  cy_ = cy;
  intensity_ = intensity;

  /* transformation is a pointer over a function.
   * it will contain the chosen transfo */
  float (*transformation)(uint32_t x, uint32_t y);

  /* We affect it's value to transformation */
  switch(type){
    case GAUSSIAN:
      transformation = &gaussian_;
      break;
    case SPHERICAL:
      transformation = &spherical_;
      break;
    case PARABOLIC:
      transformation = &parabolic_;
      break;
    case PHYSICAL:
      transformation = &physical_;
      break;
    case POWER:
      transformation = &power_;
      break;
    case GAMMA02:
        transformation = &gamma02_;
        break;
    default:
      std::cerr << "Transformation non renseignee" << std::endl;
      return 1;
      break;
  }

  /* We effectively apply the choosen transfo  */
  for(size_t y = 0; y < height_ ; y++)
    for(size_t x = 0; x < width_ ; x++){
      /* We need to "mirror" with 255-x for some reasons
       * (la preuve est laissee en exercice au lecteur) */
      img.at<uchar>(y,x) = 255-transformation(x,y)*(255-img.at<uchar>(y,x));
    }

  return 0;
}

int ISOPressure::visualize(Mat img, float intensity,
                    uint32_t cx, uint32_t cy,
                    PType type){

    Mat canvas(img.size().height, img.size().width, CV_8UC(1), Scalar(0));
    apply(canvas, intensity, cx, cy, type);

    namedWindow("Transformation visualization", WINDOW_AUTOSIZE );
    imshow("Transformation visualization", canvas);

    waitKey(0);
}
