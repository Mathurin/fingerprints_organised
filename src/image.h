/**
 *  \file	image.h
 *  \brief	Implement a class Image.
 *
 */

#ifndef IMAGE_H
#define IMAGE_H

/* Opencv includes */
#include <opencv2/opencv.hpp>
#include "isopressure.h" /* Just for the enum PType...possible to be more efficient */
#include "structuring_element.h"
using namespace cv;


//! ENERGY_FUNCTION enum
/*! All different energy function */
enum ENERGY_FUNCTION {
    LINEAR, /*!< Linear function. */
    QUADRATIC, /*!< Quadratic function. */
    E_GAUSS /*!< Gaussian function. */
    };
//! KERNEL_TYPE enum
/*! All different kernel used */
enum KERNEL_TYPE {
    ID, /*!< Neutral Kernel. */
    MEDIUM, /*!< Medium Kernel. */
    TRIANGLE, /*!< Triangle Kernel. */
    PARABOLA, /*!< Parabolic Kerlel. */
    K_GAUSS /*!< Gaussian Kernel. */
    };

/**
 * \class Image
 * \brief	A class to store an image.
 * This class attributes contain a matrix of pixels (representing an image), it
 * width and length.
 * This class has methods implementing useful operations on images as loading,
 * saving, etc...
 */
class Image{
private:
    /** The width of the image.
    */
    unsigned int width;
    /** The height of the image.
    */
    unsigned int height;
    /** The matrix containing the pixels of the image.
     */
    Mat matrix;

public:
    /**
     * \brief	A constructor of the class Image.
     *
     * @param   m	A matrix of pixels of type Mat.
     *
     * @return  The image corresponding to the pixels provided.
     */
    Image(Mat m);
    /**
     * \brief	A constructor of the class Image.
     *
     * @param   filename	The directory of an image.
     *
     * @return  The image stored at the directory provided.
     */
    Image(const string& filename);
    /**
     * \brief	A constructor of the class Image.
     *
     * @param w The width of the image.
     *
     * @param h The height of the image.
     *
     * @return  A white image.
     */
    Image(unsigned int w, unsigned int h);
    /**
     * \brief	A getter for the matrix.
     *
     * @return  The matrix of type Mat.
     */
    Mat get_matrix()const;
    /**
     * \brief	A method to access a pixel of the image in convention : float in [0,1].
     *
     * @param x The abcissa of the pixel.
     *
     * @param y The ordinate of the pixel.
     *
     * @return  The pixel of coordinates (x,y) of the image.
     */
    double get_float_pixel(unsigned int x, unsigned int y)const;
    /**
     * \brief	A method to access a pixel of the image in convention : unsigned int in [0,255].
     *
     * @param x The abcissa of the pixel.
     *
     * @param y The ordinate of the pixel.
     *
     * @return  The pixel of coordinates (x,y) of the image.
     */
    unsigned int get_int_pixel(unsigned int x, unsigned int y)const;
    /**
     * \brief	A method to display an image.
     *
     * @param   title   The title which will be displayed with the image.
     *
     * @param   time   The duration of displaying in secondes.
     */
    void display_image(const string& title, unsigned int time);
    /**
     * \brief	A method to compute the difference between two images.
     *
     * @param   m   The matrix of pixels of the second image.
     *
     * @return  The difference between the two images, normalise on [0,255].
     */
    Image diff_contraste(Mat m);
    /**
     * \brief	A method to display the pixels of an image.
     */
    void display_pixels();
    /**
     * \brief	A method to display some informations about the image (depth,
     * type of values stored, size = (width, height)).
     */
    void display_informations();
    /**
     * \brief	A method to save an image.
     *
      * @param	directory    The directory of the image saved.
     */
    void save_image(const string& directory);
    /**
     * \brief	A method to draw a unicolor rectangle (with the color convention :
     * black = 0, white = 255) on an image.
     *
     * @param  begin_x  Abcissa where the rectangle begins.
     *
     * @param end_x Abcissa where the rectangle ends.
     *
     * @param begin_y   Ordinate where the rectangle begins.
     *
     * @param end_y Ordinate where the rectangle ends.
     *
     * @param color Color of the rectangle.
     *
     * @return  The new image with the rectangle drew on it.
     */
    Image draw_rectangle(unsigned int begin_x, unsigned int end_x, unsigned int begin_y, unsigned int end_y, unsigned int color);
    /**
     * \brief	A method to cast the value of every pixel from [0, 255] (unsigned
     * int) to [0, 1] (float).
     *
     * @return  The new image which matrix values are float in [0, 1].
     */
    Image cast_into_float();
    /**
     * \brief	A method to cast the value of every pixel from  [0, 1] (float) to
     * [0, 255] (unsigned int).
     *
     * @return  The new image which matrix values are unsigned int in [0, 255].
     */
    Image cast_into_int();
    /**
     * \brief	A method to multiply two images considered as the matrix of their pixels.
     *
     * @pre   Both images should have matrix of uint in [0, 255].
     *
     * @param img   The second image needed for the product.
     *
     * @return  The "product" image.
     */
    Image mult(Image img);
    /**
     * \brief	A method to obtain the symmetric along the y axis of an image.
     *
     * @return  The symmetrized image.
     */
    Image symmetry_along_y_axis();
    /**
     * \brief	A method to obtain the symmetric along the x axis of an image.
     *
     * @return  The symmetrized image.
     */
    Image symmetry_along_x_axis();
    /**
     * \brief	A method to obtain the symmetric along the x and y (first)diagonal of an image
     * (i.e. it corresponds to the transpose of the image).
     *
     * @return  The transposed image.
     */
    Image symmetry_along_diag1();
    /**
     * \brief	A method to obtain the symmetric along the y and x (second) diagonal of an image
     *
     * @return  The symmetric along the second diagonal of the image.
     */
    Image symmetry_along_diag2();
    /**
     * \brief	A method to rotated an image with a certain angle.
     *
     * @param angle The angle of rotation.
     *
     * @return  The matrix of the rotated image.
     */
    Mat rotated_image(float angle);
    /**
     * \brief	A method to translate an image.
     *
     * @param T  The vector of translation.
     *
     * @return  The matrix of the translated image.
     */
    Mat translated_image(vector<float> T);
    /**
     * \brief	A method to compare this image with a matrix of another image.
     *
     * @pre   Both images should have matrix of uint in [0, 255].
     *
     * @param matrix  The matrix of the image we want to compare this image to.
     *
     * @return  a percentage of accuracy.
     */
    float compare_image(Mat matrix);
    /**
     * \brief	A method to compare this image with a matrix of another image.
     *
     * @pre   Both images should have matrix of double in [0, 1].
     *
     * @param matrix1  The matrix of the image we want to compare this image to.
     *
     * @return  a percentage of accuracy.
     */
    double compare_double_image(Mat matrix1);

    /*** PRESSURE OPERATIONS ***/
    /**
     * \brief	A method to simulate a lack of pressure during acquisition (isotropic)
     *
     * @param intensity  Intensity of the pressure, between 0 (no pressure) to 1 (normal pressure)
     *
     * @param cx  Center of maximal pressure (abcissa)
     *
     * @param cy  Center of maximal pressure (ordinate)
     *
     * @param type  In GAUSSIAN, PHYSICAL, SPHERICAL, GAMMA02
     *
     * @return  1 if success, 0 otherwie
     */
    int isotropic_pressure(float intensity, int cx, int cy, PType type);
    /**
     * \brief	A method to simulate a lack of pressure during acquisition (anisotropic)
     *
     * @param intensity  Intensity of the pressure, between 0 (no pressure) to 1 (normal pressure)
     *
     * @param cx  Center of maximal pressure (abcissa)
     *
     * @param cy  Center of maximal pressure (ordinate)
     *
     * @return  1 if success, 0 otherwie
     */
    int anisotropic_pressure(float intensity, int cx, int cy);
    /**
     * \brief	A method to darken the ridges of a fingerprint.
     *
     * @param center The center of the fingerprint.
     *
     * @return  The darkened image.
     */
    Mat darken_image(vector<float> center);
    /**
     * \brief	A method to locally rotate a fingerprint.
     *
     * @param center The center of the fingerprint.
     *
     * @param strength  The strength with which we want to warp the image.
     *
     * @return  The matrix of the locally rotated image.
     */
    Mat locally_rotate_image(vector<float> center, float strength);
    /**
     * \brief	A method to locally translate a fingerprint.
     *
     * @param center The center of the fingerprint.
     *
     * @param strength  The strength with which we want to warp the image.
     *
     * @return  The matrix of the locally translated image.
     */
    Mat locally_translate_image(vector<float> center, float strength);
    /**
     * \brief	A method to squeeze the valleys of a fingerprint.
     *
     * @param center The center of the fingerprint.
     *
     * @return  The valley squeezed image.
     */
    Mat squeeze_image(vector<float> center);
    /**
     * \brief	A method to create the effect of pressuring and twisting the finger
     * during the fingerprint acquisition.
     *
     * @param center The center of the fingerprint.
     *
     * @param strength The strength with which we want to modify our image.
     *
     * @return  The matrix of the transformed image.
     */
    Mat warp_image(vector<float> center, float strength);
    /**
     * \brief	A method to transform a grayscale image into a full black or white image.
     *
     * @return  The binarized image.
     */
    Image binarize();
    /**
     * \brief	A method to dilate a binarized image.
     *
     * @param str The type of the structuring element that will be used.
     *
     * @param size The size of the structuring element.
     *
     * @return  The dilated image.
     */
    Image dilate(Structure str, int size);
    /**
     * \brief	A method to erose a binarized image.
     *
     * @param str The type of the structuring element that will be used.
     *
     * @param size The size of the structuring element.
     *
     * @return  The erosed image.
     */
    Image erose(Structure str, int size);
    /**
     * \brief	A method to dilate a grayscale image.
     *
     * @param str The type of the structuring element that will be used.
     *
     * @param size The size of the structuring element.
     *
     * @return  The dilated grayscale image.
     */
    Image grayscale_dilate(Structure str, int size);
    /**
     * \brief	A method that performes a dilation focused on the center of
     * the fingerprint onto a grayscale image.
     *
     * @param center The center of the fingerprint.
     *
     * @return  The dilated image.
     */
    Image real_grayscale_dilate(vector<float> center);
    /**
     * \brief	A method to erose a grayscale image.
     *
     * @param str The type of the structuring element that will be used.
     *
     * @param size The size of the structuring element.
     *
     * @return  The erosed grayscale image.
     */
    Image grayscale_erose(Structure str, int size);
    /**
     * \brief	A method that performes an erosion focused on the center of
     * the fingerprint onto a grayscale image.
     *
     * @param center The center of the fingerprint.
     *
     * @return  The dilated image.
     */
    Image real_grayscale_erose(vector<float> center);
    /**
     * \brief	A method to hit-or-miss a binarized image.
     *
     * @return a hit-or-missed image.
     */
    Image hit_or_miss();
    /**
     * \brief	A method to squeletonize a binarized image.
     *
     * @return  the squeleton of the image.
     */
    Image squeletonize();
    /**
     * \brief	A method to hit-or-miss a binarized image.
     *
     * @param elt The type of the composite element that will be used.
     *
     * @return a hit-or-missed image.
     */
    Mat hit_or_miss(CompositeElement elt);
    /**
     * \brief	A method to obtain the convolution between the image and a given kernel.
     *
     * @param kernel  The kernel to convolve the image.
     *
     * @param borderType  The border convention chosed to fill the border.
     *
     * @param value_ct  In case of constant border, its value (0 by default).
     *
     * @param cast_in_int  If true, the image returned is in int convention, else it is in float convention.
     *
     * @return  The convolution.
     */
    Image convolution_spatial(Mat kernel, int borderType, int value_ct=0, bool cast_in_int=true);
    /**
     * \brief	A method to get the optimized size to compute the FFT.
     *
     * @param kernel The kernel used for the convolution.
     *
     * @return  The optimized size.
     */
    Size get_opti_dft_size(Mat kernel);
    /**
     * \brief	A method to compute the product term by term of two image.
     *
     * @param kernel  The second image.
     *
     * @return  The image product.
     */
    Image mult_term_by_term(Image kernel);
    /**
     * \brief	A method which compute the discrete Fourier transform of an image.
     *
     * @param non_zero_row  The number of non nul rows(to avoid useless computations).
     *
     * @param complex   If true, the output is a complex matrix. By default, it is
     * set to false so that the output of a real matrix is stored into a real matrix
     *  with a particular convention.
     *
     * @return  The discrete Fourier transform of the image.
     */
    Image DFT(unsigned int non_zero_row, bool complex=false);
    /**
     * \brief	 A method which compute the inverse discrete Fourier transform of an image.
     *
     * @param non_zero_row  The number of non nul rows(to avoid useless computations).
     *
     * @param complex   If true, the output is a complex matrix. By default, it is
     * set to false so that the output will be a real matrix. It needs that the input is the
     * the discrete Fourier transform of a real image.
     *
     * @return  The inverse discrete Fourier transform of the image.
     */
    Image IDFT(unsigned int non_zero_row, bool complex=false);
    /**
     * \brief	A method to resize an image. Put the image into a larger frame
     * or cut a part of the image.
     *
     * @param frame  The part of the image being cut or the size of the larger frame.
     *
     * @param border_color  The color of the extra border if we increase the size of the image.
     */
    Image resized(Rect frame, unsigned int border_color=0);
    /**
     * \brief	A method to give a periodize image (concatenate the symmetry along x
     * and then the symmetry along y).
     *
     * @return  The periodized image (of area multiplied by 4).
     */
    Image periodized();
    /**
     * \brief	A method to obtain the convolution of an image with a given kernel
     * using the discrete Fourier transform to enhance complexity.
     *
     * @param kernel  The kernel to which the image is convolved.
     *
     * @param cast_in_int  If true, the image returned is in int convention, else it is in float convention.
     *
     * @return  The convolution.
     */
    Image convolution_frequency(Mat kernel, bool cast_in_int=true);
    /**
     * \brief	A method to obtain the deconvolution of an image with a given kernel
     * using the discrete Fourier transform to enhance complexity.
     *
     * @param kernel  The kernel to which the image has been convolved.
     *
     * @return  The deconvolution.
     */
    Image deconvolution_frequency(Mat kernel);
    /**
     * \brief	A method to binarize an image using a threshold.
     *
     * @param mat  the matrix where we want to store the result.
     *
     * @param threshold  the threshold of binarization.
     */
    void binarize_with_threshold(Mat& mat, float threshold);

    /**
     * \brief	A method to binarize an image using a threshold.
     *
     * @param threshold  the threshold of binarization.
     *
     * @return  The binarized image.
     */
    Image binarize_with_threshold(float threshold);

    /**
     * \brief	A method to prune a skeletonized image by max 3 pixels. Called by Image::prune(int)
     *
     * @return  A pruned skeleton
     */
    Image prune();
    /**
     * \brief	A method to prune a skeletonized image by max 3*n pixels
     *
     * @param n the number of times pruned should be called
     *
     * @return  A pruneder skeleton
     */
    Image prune(int n);
    /**
     * \brief	A method to calculate the sum of the intensities of a rectangle
     *
     * @param start_x the abcissa of the starting point.
     *
     * @param start_y the ordinate of the starting point.
     *
     * @param size_x the width of the rectangle.
     *
     * @param size_y the height of the rectangle.
     *
     * @return  The sum of the pixels inside the rectangle.
     */
    uint calculate_intensity(uint start_x, uint start_y, uint size_x, uint size_y);
    /**
     * \brief	A method to find the center of a rectangle in a fingerprint.
     *
     * @param start_x the abcissa of the starting point.
     *
     * @param start_y the ordinate of the starting point.
     *
     * @param size_x the width of the rectangle.
     *
     * @param size_y the height of the rectangle.
     *
     * @return  The center of the fingerprint.
     */
    vector<uint> find_center(uint start_x, uint start_y, uint size_x, uint size_y);
    /**
     * \brief	A method which multiplie matrix by a coef such as sum(matrix) = valuer
     *
     * @param value Desired value for sum(matrix)
     */
     void setEnergy(double value);
    /**
     * \brief	A method which compute the maximum distance between (c_x,c_y) and any other points in the image.
     *
     * @param c_x horizontal axis.
     *
     * @param c_y vertical axis.
     *
     * @return  The maximum distance.
     */
    double distance_max(unsigned int c_x, unsigned int c_y);
    /**
     * \brief	A method which gives the energy of a Kernel with the distance and the distance max, following a linear function.
     *
     * @param distance The distance between the current pixel and the center pixel.
     *
     * @param dmax distance maximum possible inside the image.
     *
     * @return  The energy that should have the kernel.
     */
    static double linearEnergy(double distance, double dmax);
    /**
     * \brief	A method which gives the energy of a Kernel with the distance and the distance max, following a quadratic function.
     *
     * @param distance The distance between the current pixel and the center pixel.
     *
     * @param dmax distance maximum possible inside the image.
     *
     * @return  The energy that should have the kernel.
     */
    static double quadraticEnergy(double distance, double dmax);
    /**
     * \brief	A method which gives the energy of a Kernel with the distance and the distance max, following a Gaussian function.
     *
     * @param distance The distance between the current pixel and the center pixel.
     *
     * @param dmax distance maximum possible inside the image.
     *
     * @return  The energy that should have the kernel.
     */
    static double gaussEnergy(double distance, double dmax);
    /**
     * \brief	A method which create the kernel according the argument.
     *
     * @param coef Coefficient which gives how much the kernel will blur the image.
     *
     * @param KERNEL_TYPE Desired type of kernel.
     *
     * @param size Size of the kernel.
     *
     * @return  The desired kernel of type image.
     */
    static Image createKernel(double coef, int KERNEL_TYPE, Size size);
    /**
     * \brief	A method to make a special convolution, where the kernel depend of where we apply the convolution.
     *
     * @param ENERGY_FUNCTION What kind of energy-function use for the kernel.
     *
     * @param c_x horizontal axis of the blur center.
     *
     * @param c_y vertical axis of the blur center.
     *
     * @param KERNEL_TYPE Desired type of kernel.
     *
     * @param kernelSize Size of the kernel.
     * 
     * @param borderType  The border convention chosed to fill the border.
     *
     * @param value_ct  In case of constant border, its value (0 by default).
     *
     * @return  The special convolution where the kernel depends of the coordinate.
     */
    Image special_convolution(int ENERGY_FUNCTION, unsigned int c_x, unsigned int c_y, int KERNEL_TYPE, Size kernelSize, int borderType, int value_ct);
    /**
     * \brief	A method which create the reverse kernel.
     *
     * @param kernel The kernel that we want the reverse.
     *
     * @return  The reverse kernel.
     */
    static Mat reverseKernel (Mat kernel);

};

#endif
