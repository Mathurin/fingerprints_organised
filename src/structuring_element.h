/**
 *  \file	structuring_element.h
 *  \brief	Implement a class StructuringElement.
 *
 */

 #ifndef STRUCTURINGELEMENT_H
 #define STRUCTURINGELEMENT_H

 /* Opencv includes */
#include <opencv2/opencv.hpp>
#include <utility>

using namespace cv;

/**
 * An enumeration for different types of structuring elements.
 */
enum Structure {
PLUS,
SQUARE,
CIRCLE,
DIAMOND,
CROSS,
};


/**
 * \class StructuringElement
 *
 * \brief	A class to store the structuring element used in morphological operations.
 *
 */
class StructuringElement
{
public:
  /**
   * \brief	A constructor of the Class StructuringElement.
   *
   * @return 	a structuring element.
   */
  StructuringElement();
  /**
   * \brief	A constructor of the Class StructuringElement.
   *
   * @param str The type of the structuring element.
   *
   * @param size The size of the structuring element.
   *
   * @return 	a structuring element that corresponds to the structure provided.
   */
  StructuringElement(Structure str, int size);
  /**
   * \brief	A constructor of the Class StructuringElement.
   *
   * @param str ...
   *
   * @return 	a structuring element that corresponds to the string provided.
   */
  StructuringElement(Mat const& str);
  /**
   * \brief	A function to check if a point intersects with the structuring element.
   *
   * @param	x The abcissa of the point.
   *
   * @param y The ordinate of the point.
   *
   * @return 	true if the point intersects with the structuring element, false otherwise.
   */
  bool intersect(float x, float y);
  /**
   * \brief	A function to determine if a structuring element placed at (x,y)
   * overlaps with an area of 0.
   *
   * @param	img Coding for our set (binary)
   *
   * @param x Abcissa of the structuring element
   *
   * @param y Ordinate of the structuring element
   *
   * @return 	True if it overlaps, 0 otherwise
   */
  bool overlap(Mat img, float x, float y);
  /**
   * \brief	A function to compute the maximum intensity of the overlap between
   * The image and the structuring element.
   *
   * @param	img A grayscale image.
   *
   * @param x The abcissa of start of the structuring element in accordance with the image.
   *
   * @param y The ordinate of start of the structuring element in accordance with the image.
   *
   * @return 	The maximum value of intensity of said overlap.
   */
  int grayscale_overlap(Mat img, float x, float y);
  /**
   * \brief	A function to determine if a structuring element placed at (x,y)
   * overlaps with an area of 1.
   *
   * @param	img Coding for our set (binary)
   *
   * @param x Abcissa of the structuring element
   *
   * @param y Ordinate of the structuring element
   *
   * @return 	True if it underlaps, 0 otherwise
   */
  bool underlap(Mat img, float x, float y);
  /**
   * \brief	A function to compute the minimum intensity of the overlap between
   * The image and the structuring element.
   *
   * @param	img A grayscale image.
   *
   * @param x The abcissa of start of the structuring element in accordance with the image.
   *
   * @param y The ordinate of start of the structuring element in accordance with the image.
   *
   * @return 	The minimum value of intensity of said overlap.
   */
  int grayscale_underlap(Mat img, float x, float y);
  /**
   * \brief Determines if the structuring element entirely fits inside our set
   *
   * @param	img Coding for our set (binary)
   *
   * @param x Abcissa of the structuring element
   *
   * @param y Ordinate of the structuring element
   *
   * @return 	True if it fits, 0 otherwise
   */
  bool fits_inside(Mat img, int x, int y);
  /**
   * \brief Determines if the structuring element entirely fits outside our set
   *
   * @param	image Coding for our set (binary)
   *
   * @param x Abcissa of the structuring element
   *
   * @param y Ordinate of the structuring element
   *
   * @return 	True if it fits, 0 otherwise
   */
  bool fits_outside(Mat, int, int);
  /**
   * \brief	A function that computes the number of rows of a structuring element.
   *
   * @return 	The number of rows of the structuring element.
   */
  int rows();
  /**
   * \brief	A function that computes the number of columns of a structuring element.
   *
   * @return 	The number of columns of the structuring element.
   */
  int cols();
  /**
   * \brief	A function that performs a 90° clockwise rotation of a structuring element coded in a square Mat
   *
   * @return 	The rotated element
   */
  StructuringElement rotate();

private:
  /**
   * The matrix containing the pixels of the structuring element.
   */
  Mat matrix;
};

/**
 * \ Typedef	describes a composite structuring element made of two StructuringElement
 */
typedef std::pair<StructuringElement, StructuringElement> CompositeElement; //For hit-or-miss or squeletonization

/**
 * \brief	Perfoms a clockwise 90° rotation of a CompositeElement
 *
 * @param elt CompositeElement to rotate
 *
 * @return  Returns the rotated element
 */
CompositeElement rotate(CompositeElement elt);
/**
 * \brief	Creates the first Golay element
 *
 * @return  Returns the first Golay element
 */
CompositeElement create_Golay_L1();
/**
 * \brief	Creates the second Golay element
 *
 * @return  Returns the second Golay element
 */
CompositeElement create_Golay_L2();
/**
 * \brief	Creates every Golay L elements and put it in an array
 *
 * @param golay_L Array of CompositeElement of size 8
 */
void create_Golay_L(CompositeElement* golay_L);
/**
 * \brief	Creates the first pruning element
 *
 * @return  Returns the first pruning element
 */
CompositeElement create_pruning_1();
/**
 * \brief	Creates the second pruning element
 *
 * @return  Returns the second pruning element
 */
CompositeElement create_pruning_2();
/**
 * \brief	Creates every pruning elements and put it in an array
 *
 * @param pruning Array of CompositeElement of size 8
 */
void create_pruning_elements(CompositeElement* pruning);

#endif
