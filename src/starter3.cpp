/**
 *  @file	starter3.cpp
 *  @brief	Implement the convolution : first with a direct algorithm and then
 * using the discrete Fourier transform.
 *
 */

/* Standard includes */
#include <iostream>
#include <cstdint>
#include <vector>
#include <math.h>

# include "image.h"
# include "pixel.h"
using std::norm;
using std::cout;
using std::cin;
using std::endl;
using std::cerr;

Image Image::convolution_spatial(Mat kernel, int borderType, int value_ct, bool cast_in_int){
    unsigned int kh = kernel.size().height;
    unsigned int kw = kernel.size().width;
    Image img_kernel = Image(kernel);
    Mat ans = Mat::ones(height, width, CV_64F);
    int ind_x[kw];
    int ind_y[kh];
    for (unsigned int y = 0 ;y<kh;y++){
        ind_y[y] = y-kh/2;
    }
    for (unsigned int x = 0 ;x<kw;x++){
        ind_x[x] = x-kw/2;
    }
    Mat mat_border = Mat(height+kh, width+kw, matrix.type(), Scalar::all(0));
    copyMakeBorder(this->get_matrix(), mat_border, kh/2, kh-kh/2, kw/2, kw - kw/2, borderType, value_ct);
    Image img_border = Image(mat_border).cast_into_float();
    for (unsigned int y = 0; y<height;y++) {
        for (unsigned int x = 0; x<width;x++){
            double sum = 0;
            for (unsigned int ky=0; ky<kh; ky++){
                for (unsigned int kx=0; kx<kw;kx++){
                    unsigned int abs_x = x + ind_x[kx];
                    unsigned int ord_y = y + ind_y[ky];
                        sum += img_kernel.get_float_pixel(kx, ky)*img_border.get_float_pixel(abs_x + kw/2, ord_y + kh/2);
                }
            }
            ans.at<double>(y,x) = sum;
        }
    }
    Image filter_image = Image(ans);
    if (cast_in_int){
        return filter_image.cast_into_int();
    } else {
        return filter_image;
    }

}

Size Image::get_opti_dft_size(Mat kernel){
    Size dft_size;
    /* Worst */
    // dft_size.width = getOptimalDFTSize(matrix.cols+kernel.cols-1);
    // dft_size.height = getOptimalDFTSize(matrix.rows+kernel.rows-1);
    /* Better */
    dft_size.width = getOptimalDFTSize(matrix.cols);
    dft_size.height = getOptimalDFTSize(matrix.rows);
    return dft_size;
}

Image Image::mult_term_by_term(Image kernel){
    Mat product_img;
    mulSpectrums(this->matrix, kernel.matrix, product_img, 0);
    return Image(product_img);
}

Image Image::DFT(unsigned int non_zero_row, bool complex){
    Mat fft_mat;
    matrix.copyTo(fft_mat);
    if (!complex){
        dft(matrix, fft_mat, 0,  non_zero_row);
    } else {
        dft(matrix, fft_mat, DFT_COMPLEX_OUTPUT, non_zero_row);
    }

    return Image(fft_mat);
}

Image Image::IDFT(unsigned int non_zero_row, bool complex){
    Mat ift_mat;
    matrix.copyTo(ift_mat);
    if (!complex){
        dft(matrix, ift_mat, DFT_INVERSE + DFT_SCALE, non_zero_row);
    } else {
        dft(matrix, ift_mat, DFT_INVERSE + DFT_SCALE + DFT_REAL_OUTPUT, non_zero_row);
    }
    return Image(ift_mat);
}

Image Image::resized(Rect frame, unsigned int border_color){
    Size new_size;
    new_size.width = frame.width;
    new_size.height = frame.height;
    /* Increase the image*/
    if ((new_size.height >= height)&&(new_size.width >= width)){
        Mat resized_img = Mat(new_size, matrix.type(), Scalar::all(border_color));
        unsigned int bord_x = 0;
        unsigned int bord_y = 0;
        Mat copy_img = Mat(resized_img, Rect(bord_x,bord_y,matrix.cols, matrix.rows));
        matrix.copyTo(copy_img);
        return Image(resized_img);
    }
    /* Cut the image */
    else if ((new_size.height < height)&&(new_size.width < width)&& (frame.x >= 0) &&(frame.y>= 0)){
        Mat result;
        matrix(frame).copyTo(result);
        return Image(result);
    }
    else {
        cerr << "Pb of resizing !" << endl;
    }
}

Image Image::periodized(){
    Mat m;
    matrix.copyTo(m);
    Image m1 = Image(m);
    Image m2 = m1.symmetry_along_x_axis();
    Mat m_col1 = Mat(2*height, width, CV_8U, Scalar(255));
    vconcat(m1.get_matrix(), m2.get_matrix(), m_col1);
    Image m3 = m1.symmetry_along_y_axis();
    Image m4 = m3.symmetry_along_x_axis();
    Mat m_col2 = Mat(2*height, width, CV_8U, Scalar(255));
    vconcat(m3.get_matrix(), m4.get_matrix(), m_col2);
    Mat m_full = Mat(2*height, 2*width, CV_8U, Scalar(255));
    hconcat(m_col1, m_col2, m_full);
    return Image(m_full);
}

Image Image::convolution_frequency(Mat kernel, bool cast_in_int){
    Image img_periodic = this->periodized();
    Image img_kernel = Image(kernel);
    Size dft_size = img_periodic.get_opti_dft_size(kernel);
    Image resized_img = img_periodic.resized(Rect(0,0, dft_size.width, dft_size.height));
    Image resized_kernel = img_kernel.resized(Rect(0,0, dft_size.width, dft_size.height));
    resized_img.matrix.convertTo(resized_img.matrix, CV_64F);
    resized_kernel.matrix.convertTo(resized_kernel.matrix, CV_64F);

    resized_img = resized_img.DFT(img_periodic.height);
    resized_kernel = resized_kernel.DFT(img_kernel.height);

    Image product = resized_img.mult_term_by_term(resized_kernel);
    Image inverse = product.IDFT(img_periodic.height);

    Image result = inverse.resized(Rect((kernel.cols-1)/2, (kernel.rows-1)/2, this->width, this->height), 0);

    normalize(result.get_matrix(), result.get_matrix(), 1, 0,NORM_MINMAX);
    if (cast_in_int){
        return result.cast_into_int();
    } else {
        return result;
    }
}

Image Image::deconvolution_frequency(Mat kernel){
    /* Very poors results*/
    Image img_periodic = this->periodized();
    Image img_kernel = Image(kernel);
    Size dft_size = img_periodic.get_opti_dft_size(kernel);
    Image resized_img = img_periodic.resized(Rect(0,0, dft_size.width, dft_size.height), 0);
    Image resized_kernel = img_kernel.resized(Rect(0,0, dft_size.width, dft_size.height), 0);

    resized_img.matrix.convertTo(resized_img.matrix, CV_64F);
    resized_kernel.matrix.convertTo(resized_kernel.matrix, CV_64F);

    resized_kernel.get_matrix() = 1.0f/resized_kernel.get_matrix();

    resized_img = resized_img.DFT(img_periodic.height);
    resized_kernel = resized_kernel.DFT(img_kernel.height);

    Image product = resized_img.mult_term_by_term(resized_kernel);
    Image inverse = product.IDFT(img_periodic.height);
    Image result = inverse.resized(Rect((kernel.cols-1)/2, (kernel.rows-1)/2, this->width, this->height));

    normalize(result.get_matrix(), result.get_matrix(), 1, 0,NORM_MINMAX);
    Image final = result.cast_into_int();
    return final;
}
