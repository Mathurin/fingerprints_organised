#include "minutiae.h"
#include "functions.h"

unsigned int get_neighbours(Mat mat, int x, int y){
  unsigned int n = 0; //Number of neighbours

  if(mat.at<uchar>(y, x) != 0)
    return 0;

  n += abs(mat.at<uchar>(y-1, x) - mat.at<uchar>(y-1, x-1));
  n += abs(mat.at<uchar>(y-1, x+1) - mat.at<uchar>(y-1, x));
  n += abs(mat.at<uchar>(y, x+1) - mat.at<uchar>(y-1, x+1));
  n += abs(mat.at<uchar>(y+1, x+1) - mat.at<uchar>(y, x+1));
  n += abs(mat.at<uchar>(y+1, x) - mat.at<uchar>(y+1, x+1));
  n += abs(mat.at<uchar>(y+1, x-1) - mat.at<uchar>(y+1, x));
  n += abs(mat.at<uchar>(y, x-1) - mat.at<uchar>(y+1, x-1));
  n += abs(mat.at<uchar>(y-1, x-1) - mat.at<uchar>(y, x-1));

  return n/510; //n/255/2
}

Mat create_CN_matrix(Image squeletonized){
  int rows = squeletonized.get_matrix().rows;
  int cols = squeletonized.get_matrix().cols;
  Mat CN_matrix = Mat(rows, cols, CV_8UC1, Scalar(0));
  Mat s_matrix = squeletonized.get_matrix();

  /* We ignore the border of the image to
  avoid the issues created by border */
  for(int i = 1; i < rows-1; i++)
    for(int j = 1; j < cols-1; j++)
      CN_matrix.at<uchar>(i,j) = get_neighbours(s_matrix, j, i);

  return CN_matrix;
}

void display_minutiae(Image& fingerprint){
  Image binarized = fingerprint.binarize_with_threshold(70);
  Image skeleton = binarized.squeletonize().prune().prune().prune();
  Mat CN_mat = create_CN_matrix(skeleton);
  int rows = fingerprint.get_matrix().rows;
  int cols = fingerprint.get_matrix().cols;
  Mat minutiae;

  //cvtColor(fingerprint.get_matrix(), minutiae, CV_GRAY2RGB);// Convert a single-channel image to a 3-chanel rgb one
  cvtColor(fingerprint.get_matrix(), minutiae, CV_GRAY2RGB);

  for(int i = 0; i < rows; i++)
    for(int j = 0; j < cols; j++){
      if(CN_mat.at<uchar>(i,j) == 1)
        circle(minutiae, Point(j,i), 2, Scalar(255,0,0), -1, CV_AA);
      if(CN_mat.at<uchar>(i,j) == 3)
        circle(minutiae, Point(j,i), 2, Scalar(0,0,255), -1, CV_AA);
    }

  skeleton.display_image("Skeleton", 0);
  binarized.display_image("Binarized", 0);
  namedWindow("Minutiae detection", WINDOW_AUTOSIZE );
  imshow("Minutiae detection", minutiae);

  //skeleton.save_image("./skeleton_pruned_2.jpg");
  //Image(minutiae).save_image("./minutiae_pruned_2.jpg");
  waitKey(0);
}
