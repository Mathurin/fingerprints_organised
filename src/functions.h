/**
 *  @file	functions.h
 *  @brief	Implement several functions used in the project.
 *
 */

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "structuring_element.h"
/* Opencv includes */
#include <opencv2/opencv.hpp>
using namespace cv;


/**
 * \brief	A function to computes the distance between two points.
 *
 * @param	x Abcissa of the first point.
 *
 * @param y Ordinate of the first point.
 *
 * @param center  The other point.
 *
 * @return  The value of the distance between the two points.
 */
float distance(float x, float y, vector<float> & center);
/**
 * \brief	A function fill the intersection of the structuring element
 * and the image with the intensity color.
 *
 * @param	tmp the matrix of the image.
 *
 * @param structure the structuring element.
 *
 * @param	x Abcissa of the first point of the structuring element in the image.
 *
 * @param y Ordinate of the first point of the structuring element in the image.
 *
 * @param color  The intensity that we will use to fill out the intersection.
 */
void fill(Mat& tmp, StructuringElement structure, int x, int y, float color);
/**
 * \brief	A function to compute the coordinates of a square surrounding a point.
 *
 * @param	coord the coordinates of the point.
 *
 * @return 	a vector of the four coordinates surrounding the point forming a square.
 */
vector<vector<float> > square(vector<float> coord);
/**
 * \brief	A function to compute the rotated coordinates of a point.
 *
 * @param	x The abcissa of the point.
 *
 * @param y The ordinate of the point.
 *
 * @param angle The angle of rotation.
 *
 * @param center The center of rotation.
 *
 * @return 	The rotated coordinates of a point.
 */
vector<float> rotate(float x, float y, float angle, vector<float> & center);
/**
 * \brief	A function to compute the translated coordinates of a point.
 *
 * @param	x The abcissa of the point.
 *
 * @param y The ordinate of the point.
 *
 * @param T The vector of translation.
 *
 * @return The translated coordinates of a point.
 */
vector<float> translate(float x, float y, vector<float> T);
/**
 * \brief	A function to compute the fous integer neighbouring coordinates of a
 * Real point.
 *
 * @param	coord The coordinates of the point.
 *
 * @return 	A vector of the neighbouring integer coordinates.
 */
vector<vector<float> > coord_neighbours(vector<float> coord);
/**
 * \brief	A function to check if a point is inside the image.
 *
 * @param	img The matrix of the image.
 *
 * @param	v The vector of coordinates.
 *
 * @return 	true if the point is inside the image, false otherwise.
 */
bool Isinside(Mat img, vector<float> v);
/**
 * \brief	A function to to check if a point is inside the image.
 *
 * @param	img The matrix of the image.
 *
 * @param	x The abcissa of the point.
 *
 * @param y The ordinate of the point.
 *
 * @return  true if the point is inside the image, false otherwise.
 */
bool Isinside_(Mat img,float x, float y);
/**
 * \brief	A function to check if a point is of real coordinates or of integer ones.
 *
 * @param	coord The coordinates of the point.
 *
 * @return 	true if the coordinates are integer, false otherwise.
 */
bool Areinteger(vector<float> coord);
/**
 * \brief	A function to translate the coordinates of a point towards the center.
 *
 * @param	x The abcissa of the point.
 *
 * @param y The ordinate of the point.
 *
 * @param center The coordinates of the center.
 *
 * @return 	The new transformed coordinates.
 */
vector<float> warp(float x, float y, vector<float> center);
/**
 * \brief	A function that computes an angle depending on the distance of the pixel
 * and the strength provided.
 *
 * @param	dist The distance between the center of the fingerprint and the pixel.
 *
 * @param strength The strength of the local rotation.
 *
 * @return 	The relative angle.
 */
float relative_angle(float dist, float strength);
/**
 * \brief	A function that computes an vector depending on the distance of the pixel
 * and the strength provided.
 *
 * @param	dist The distance between the center of the fingerprint and the pixel.
 *
 * @param strength The strength of the local translation.
 *
 * @return 	The relative translation vector.
 */
vector<float> relative_translation_vector(float dist,float strength);
/**
 * \brief	A function that darkens a pixel depending on its intensity.
 *
 * @param	intensity The intensity of the pixel.
 *
 * @param	dist The distance between the center of the fingerprint and the pixel.
 *
 * @return 	The new darkened intensity.
 */
float darken_intensity(float intensity, float dist);
/**
 * \brief	A function that checks term by term if two matrices are equal.
 *
 * @param	A The first matrix.
 *
 * @param B The second matrix.
 *
 * @return 	true if the two matrices are equal, false otherwise.
 */
bool areEqual(const Mat& A, const Mat& B);
/**
 * \brief	A function that returns the size of the structuring element depending on the
 * distance of the pixel from the center of the fingerprint for the dialtion operation.
 *
 * @param	dist The distance between the center of the fingerprint and the pixel.
 *
 * @return 	The appropriate size of the structuring element.
 */
int dilate_size_coef(float dist);
/**
 * \brief	A function that returns the size of the structuring element depending on the
 * distance of the pixel from the center of the fingerprint for the erosion operation.
 *
 * @param	dist The distance between the center of the fingerprint and the pixel.
 *
 * @return 	The appropriate size of the structuring element.
 */
int erose_size_coef(float dist);
/**
 * \brief	A function that returns the type of the structuring element depending on the
 * distance of the pixel from the center of the fingerprint for the erosion operation.
 *
 * @param	dist The distance between the center of the fingerprint and the pixel.
 *
 * @return 	The appropriate type of the structuring element.
 */
Structure erose_str_coef(float dist);
/**
 * \brief	A function that returns the type of the structuring element depending on the
 * distance of the pixel from the center of the fingerprint for the dilation operation.
 *
 * @param	dist The distance between the center of the fingerprint and the pixel.
 *
 * @return 	The appropriate type of the structuring element.
 */
Structure dilate_str_coef(float dist);
/**
 * \brief	A function that perfoms the set difference A-B where the set is represented by 0s
  Suppose that the matrix are in {0,255} and the same size.
 *
 * @param	A The first matrix
 *
 * @param	B The sceond matrix
 *
 * @return 	The set difference A-B.
 */
Mat set_difference(Mat A, Mat B);
/**
 * \brief	 A function that perfoms the set union AUB where the set is represented by 0s
   Suppose that the matrix are in {0,255} and the same size
 *
 * @param	A The first matrix
 *
 * @param	B The sceond matrix
 *
 * @return 	The setunion AUB.
 */
Mat set_union(Mat A, Mat B);
/**
 * \brief	A function that perfoms  the set intersection AnB where the set is represented by 0s
   Suppose that the matrix are in {0,255} and the same size.
 *
 * @param	A The first matrix
 *
 * @param	B The sceond matrix
 *
 * @return 	The set intersection AnB.
 */
Mat set_intersection(Mat A, Mat B);
#endif
