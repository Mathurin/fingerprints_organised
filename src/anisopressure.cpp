/**
 *  @file	anisopressure.cpp
 *  @brief	Implement some static methods (i.e some functions) of the class ANISOPressure.
 *
 */

#include "anisopressure.h"

// what is the usefulness of the 3 following lines ?
uint32_t ANISOPressure::height_, ANISOPressure::width_;
float **ANISOPressure::mask;

int ANISOPressure::apply(Mat img){

  /* We affect the values to our member variables */
  height_ = img.size().height;
  width_  = img.size().width;


  /* We effectively apply the choosen transfo  */
  for(size_t y = 0; y < height_ ; y++)
    for(size_t x = 0; x < width_ ; x++){
      /* We need to "mirror" with 255-x for some reasons
       * (la preuve est laissee en exercice au lecteur) */
      img.at<uchar>(y,x) = 255-mask[x][y]*(255-img.at<uchar>(y,x));
    }

  return 0;
}

bool ANISOPressure::inImage(int x, int y){
  return 0 <= x && x < width_ && 0 <= y && y < height_;
}

void ANISOPressure::diffusion(int cx, int cy){
  /*
   * We fill mask by diffusion.
   * Would be better to implement scanline algorithm
   * cy and cy are the center of the diffusion
   * recursive version : problem of stack overflow
   */
  std::stack<std::pair<int, int>> P;

  if(inImage(cx, cy) && mask[cx][cy] == 0.){
        P.push(std::make_pair(cx, cy));
  }

  int x, y;

  while(!P.empty()){
    std::tie(x,y) = P.top();
    P.pop();

    if(inImage(x,y)){
      mask[x][y] = 1.;

      if(inImage(x-1,y) && mask[x-1][y] == 0.)
        P.push(std::make_pair(x-1, y));
      if(inImage(x+1,y) && mask[x+1][y] == 0.)
        P.push(std::make_pair(x+1, y));
      if(inImage(x,y-1) && mask[x][y-1] == 0.)
        P.push(std::make_pair(x, y-1));
      if(inImage(x,y+1) && mask[x][y+1] == 0.)
        P.push(std::make_pair(x, y+1));

    }
  }
}

void ANISOPressure::blurMask(){
  /*
   * blur the mask
   */
   for(int x = 1; x < width_-1; x++)
    for(int y = 1; y < height_-1; y++){
      /* TODO : warning : values in x-1 and y1 have already
      been modofied, should work on a copy  */
      mask[x][y] = (mask[x-1][y] + mask[x+1][y] + mask[x][y+1] + mask[x][y-1] + mask[x][y])/5;
    }
}

float ANISOPressure::sumWeight(float* dr, int n, int N, int I){
  float sum = 0;
  for(int i = 0; i < I; i += 1){
    sum += inverseDistanceWeight(n, i*N/I);
  }
  sum += inverseDistanceWeight(n, N);
  return sum;
}

float ANISOPressure::weightedPoints(float* dr, int n, int N, int I){
  float sum = 0;
  for(int i = 0; i < I; i += 1){
    sum += inverseDistanceWeight(n, i*N/I)*dr[i*N/I];
  }
  sum += inverseDistanceWeight(n, N)*dr[0]; //To have a closed curve
  return sum;
}

void ANISOPressure::shepardInterpolation(float* dr, int N, int I){
  float denominator, numerator;

  for(int n = 0; n < N ; n++){
    if(n%(N/I) == 0) //We don't want to affect the fixed points
      continue;

    denominator = sumWeight(dr, n, N, I);
    numerator = weightedPoints(dr, n, N, I);
    dr[n] = numerator/denominator;
  }
}

void ANISOPressure::createPerturbation(float* dr, int N, int irregularity, int max_deformation){
  /*
   * Creates the perturbation of the radius
   * of the ellipse
   * irregularity (have to divide N)
   * Represents the quantity of oscillations
   * we will see on the ellipse
   * max_deformation Represents the amplitude of
   * the oscillations we will see on
   * the ellipse (in pixel) */

   if(N%irregularity != 0){
     std::cout << "irregularity has to divide N" << std::endl;
     return;
   }
   /* We create the "sampled" points */
   int between = N/irregularity;
   for(int i = 0; i < irregularity; i++)
     dr[i*between] = max_deformation*(float)(rand()-RAND_MAX/2)/RAND_MAX;

   /* We interpolate those points */
   shepardInterpolation(dr, N, irregularity);
}

float ANISOPressure::inverseDistanceWeight(float x, float y){
  return 1./((x-y)*(x-y));
}

void ANISOPressure::drawOnMask(float* dr, float a, float b, int cx, int cy, int N){
  float step = 6.28318530718/N; //2*PI/N
  int x, y;
  float theta;

  for(int i = 0; i < N; i++){
    theta = i*step;
    x = round((a+dr[i])*cos(theta))+cx;
    y = round((b+dr[i])*sin(theta))+cy;

    if(inImage(x,y))
      mask[x][y] = 1.;
  }
}

void ANISOPressure::drawOnMask2(float* dr, float a, float b, int cx, int cy, int N, int stroke){
  float step = 6.28318530718/N; //2*PI/N
  int x, y;
  float theta;

  for(int i = 0; i < N; i++){
    theta = i*step;
    for(int j = 1; j < stroke; j++){
      x = round((a+j+dr[i])*cos(theta))+cx;
      y = round((b+j+dr[i])*sin(theta))+cy;

      if(inImage(x,y))
        mask[x][y] = .8;
    }
  }
}

int ANISOPressure::generateMask(Mat image, float intensity, const int cx, const int cy){
  /*
   * Le principe est de dessiner une ellipse,
   * de la perturber, puis de flouter les bords
   */
  height_ = image.size().height;
  width_  = image.size().width;


  /* We create a new array and set
  every value to 0 */
  mask = new float*[width_];
  for(int x = 0; x < width_ ; x++){
    mask[x] = new float[height_];
    memset(mask[x], 0, sizeof(mask[x][0])*height_);
  }

  /* Variables describing the ellipse */
  float a = width_/2*intensity;//half greatest axis of the ellipse
  float b = height_/1.5*intensity;//half shortest axis of the ellipse

  int irregularity1 = 10;
  int irregularity2 = 15;
  int N = 100*irregularity1*irregularity2; //Number of steps (discretization)
  int amplitude = intensity*width_/7; //Has to depend on the size and intensity
  int stroke = width_/50;

  float dr[N]; //"Perturbation" array
  float dr2[N]; //Perturbation of the perturbation...pertubaception
  float dr3[N];

  createPerturbation(dr, N, irregularity1, amplitude);
  drawOnMask(dr, a, b, cx, cy, N);
  diffusion(cx, cy);

  createPerturbation(dr2, N, irregularity2, amplitude/2);
  std::transform(dr, dr+N, dr2, dr3, std::plus<int>()); //Compute the sum of dr and dr2 and put it in dr3
  drawOnMask2(dr3, a, b, cx, cy, N, stroke);

  for(int i = 0; i < width_/70; i++) //The bigger the image, the more we have to smooth
    blurMask();
}

int ANISOPressure::maskFromBitmap(String path){
  Mat image;
  image = imread( path, CV_LOAD_IMAGE_GRAYSCALE);

  if ( !image.data )
  {
      std::cout << "No image data \n" << std::endl;
      return -1;
  }

  height_ = image.size().height;
  width_  = image.size().width;

  mask = new float*[width_];

  for(int x = 0; x < width_ ; x++){
    mask[x] = new float[height_];
    for(int y = 0; y < height_; y++){
      mask[x][y] = 1.-image.at<uchar>(y,x)/255.;
      std::cout << mask[x][y] << std::endl;
    }
  }
}

int ANISOPressure::visualize(Mat img){
    /* TODO : no image appears
    Mat canvas(img.size().height, img.size().width, CV_8UC3, Scalar(0));
    apply(canvas, intensity, cx, cy, type);

    namedWindow("Transformation visualization", WINDOW_AUTOSIZE );
    imshow("Transformation visualization", canvas);*/
    img = Scalar(0);
    apply(img);
}
